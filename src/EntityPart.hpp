#ifndef __ENTITY_PART_HPP__
#define __ENTITY_PART_HPP__

#include <string>
#include <vector>
#include <map>
#include "glm/gtc/type_ptr.hpp"

class EntityPart
{
private:
  std::string _id;
  std::string _cloneFromId;
  glm::vec4 _bboxMinNoTransform;
  glm::vec4 _bboxMaxNoTransform;
  std::vector<glm::vec4> _vertices;
  std::vector<glm::vec3> _triangles;
  std::vector<glm::vec4> _vertexNormals;
  std::vector<std::pair<float, float>> _vertexUVs;
  glm::vec4 _relativePosition;
  glm::vec3 _ambientColor;
  glm::vec3 _diffuseColor;
  glm::vec3 _specularColor;
  float _shininess;
  float _opacity;
  float _scaleFactorX;
  float _scaleFactorY;
  float _scaleFactorZ;
  float _rotationDegreesX;
  float _rotationDegreesY;
  float _rotationDegreesZ;
  bool _hasTexture;
  bool _isTextureNormalMap;
  bool _hasUV;
  bool _usePerVertexIllumination;
  std::string _textureId;
  std::map<std::string, int> _rendererIds;

public:
  EntityPart(std::string const &id, std::string const &cloneFromId = "None");

  std::string const &getId() const;
  std::string const &getCloneFromId() const;
  glm::vec3 getAmbientColor() const;
  glm::vec3 getDiffuseColor() const;
  glm::vec3 getSpecularColor() const;
  float getShininess() const;
  float getOpacity() const;
  glm::vec4 const &getVertex(int vertexNum) const;
  std::vector<glm::vec4> const &getVertices() const;
  std::vector<float> getVertices1DArray() const;
  int getVertices1DArraySize() const;
  glm::vec3 const &getTriangle(int triangleNum) const;
  std::vector<glm::vec3> const &getTriangles() const;
  std::vector<int> getTriangles1DArray() const;
  int getTriangles1DArraySize() const;
  glm::vec4 const &getVertexNormal(int vertexNum) const;
  std::vector<glm::vec4> const &getVertexNormals() const;
  std::vector<float> getVertexNormals1DArray() const;
  int getVertexNormals1DArraySize() const;
  std::pair<float, float> const &getVertexUVs(int vertexNum) const;
  std::vector<std::pair<float, float>> const &getVerticesUVs() const;
  std::vector<float> getVertexUVs1DArray() const;
  int getVertexUVs1DArraySize() const;
  glm::vec3 getBBoxMin() const;
  glm::vec4 getBBoxMinNoTranform() const;
  glm::vec3 getBBoxMax() const;
  glm::vec4 getBBoxMaxNoTranform() const;
  glm::vec4 getRelativePosition() const;
  float getScaleFactorX() const;
  float getScaleFactorY() const;
  float getScaleFactorZ() const;
  float getRotationDegreesX() const;
  float getRotationDegreesY() const;
  float getRotationDegreesZ() const;
  bool hasTexture() const;
  bool isTextureNormalMap() const;
  bool hasUV() const;
  std::string const &getTextureId() const;
  bool hasPerVertexIllumination() const;
  int getRendererId(std::string const &name) const;
  glm::mat4 getRelativeTransform() const;

  void setId(std::string const &id);
  void setCloneFromId(std::string const &cloneFromId);
  void setAmbientColor(glm::vec3 ambientColor);
  void setDiffuseColor(glm::vec3 diffuseColor);
  void setSpecularColor(glm::vec3 specularColor);
  void setShininess(float shininess);
  void setOpacity(float opacity);
  void addVertex(float x, float y, float z);
  void addVertices(std::vector<glm::vec3> const &vertices);
  void setVertices(std::vector<glm::vec3> const &vertices);
  void addTriangle(int i1, int i2, int i3);
  void addTriangles(std::vector<glm::vec3> const &triangles);
  void setTriangles(std::vector<glm::vec3> const &triangles);
  void addVertexNormal(float x, float y, float z);
  void addVertexNormals(std::vector<glm::vec3> const &vertexNormals);
  void setVertexNormals(std::vector<glm::vec3> const &vertexNormals);
  void addVertexUV(float U, float V);
  void addVertexUVs(std::vector<std::pair<float, float>> const &vertexUVs);
  void setVertexUVs(std::vector<std::pair<float, float>> const &vertexUVs);
  void setRelativePosition(float x, float y, float z);
  void setScaleFactorX(float scaleFactorX);
  void setScaleFactorY(float scaleFactorY);
  void setScaleFactorZ(float scaleFactorZ);
  void setRotationDegreesX(float rotationDegreesX);
  void setRotationDegreesY(float rotationDegreesY);
  void setRotationDegreesZ(float rotationDegreesZ);
  void resetRotation();
  void setTextureId(std::string const &textureId);
  void setTextureAsNormalMap();
  void setPerVertexIllumination();
  void setRendererId(std::string const &name, int value);
};

#endif // __ENTITY_PART_HPP__
