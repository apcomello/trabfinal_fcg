#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

#include <string>
#include "glm/mat4x4.hpp"
#include "CameraType.hpp"
#include "ProjectionType.hpp"

class Camera
{
private:
  std::string _id;
  CameraType _cameraType;
  ProjectionType _projectionType;
  float _fieldOfView;
  float _nearPlane;
  float _farPlane;
  float _aspectRatio;
  glm::vec4 _position;
  glm::vec4 _lookAtPosition;
  glm::vec4 _originalLookAtPosition;
  glm::vec4 _viewVector;
  glm::vec4 _rightVector;
  glm::vec4 _upVector;
  float _phi;
  float _theta;
  glm::vec4 _cumulativeDisplacement;
  float _cameraDistance;

public:
  Camera(std::string id);

  void displace(float x, float y, float z, float deltaMovement, float deltaTime);
  void turn(float dx, float dy, float deltaTime);

  // Getters
  std::string const &getId() const;
  float getFieldOfView() const;
  float getNearPlane() const;
  float getFarPlane() const;
  float getAspectRatio() const;
  glm::vec4 getPosition() const;
  glm::vec4 getViewVector() const;
  glm::vec4 getRightVector() const;
  glm::vec4 getUpVector() const;
  glm::vec4 getLookAtPosition() const;
  CameraType getCameraType() const;
  ProjectionType getProjectionType() const;
  glm::mat4 getViewMatrix() const;
  glm::mat4 getProjectionMatrix() const;
  float getPhi();
  float getTheta();

  // Setters
  void setId(std::string const &id);
  void setFieldOfView(float fieldOfView);
  void setNearPlane(float nearPlane);
  void setFarPlane(float farPlane);
  void setAspectRatio(float aspectRatio);
  void setPosition(float x, float y, float z);
  void setViewVector(float x, float y, float z);
  void setUpVector(float x, float y, float z);
  void setLookAtPosition(float x, float y, float z);
  void setOriginalLookAtPosition(float x, float y, float z);
  void setCameraType(CameraType cameraType);
  void setProjectionType(ProjectionType projectionType);
  void setPhi(float phi);
  void setTheta(float theta);
  void getSphericalCoordinates(float x, float y, float z);
};

#endif // __CAMERA_HPP__
