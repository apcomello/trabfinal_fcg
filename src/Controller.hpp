#ifndef __CONTROLLER_HPP__
#define __CONTROLLER_HPP__

#include "GLFW/glfw3.h"

class Controller
{
public:
  Controller(GLFWwindow &_window);
  static void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mode);
  static void MouseButtonCallback(GLFWwindow *window, int button, int action, int mods);
  static void CursorPosCallback(GLFWwindow *window, double xpos, double ypos);
  static void ScrollCallback(GLFWwindow *window, double xoffset, double yoffset);

  static double _lastCursorPosX;
  static double _lastCursorPosY;
  static double _currentCursorPosX;
  static double _currentCursorPosY;
  static bool _movingLeft;
  static bool _movingRight;
  static bool _movingForward;
  static bool _movingBackward;
  static bool _leftMouseButtonPressed;
  static bool _cameraTurning;
  static bool _showObjective;
  static bool _startGame;
  static bool _resetGame;
};

#endif // __CONTROLLER_HPP__
