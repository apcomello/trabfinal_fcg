#include <chrono>
#include <iostream>
#include <ctime>
#include <algorithm>
#include "Game.hpp"
#include "Player.hpp"
#include "Controller.hpp"
#include "CollisionHelper.hpp"
using namespace std;

Game::Game(string gameName)
    : _renderer(Renderer(gameName, 800, 800))
{
    _validGame = true;
    _objectsCollected = 0;
    _objectiveCompleted = false;
    _gameStarted = false;
}

void Game::run()
{
    World world;
    world.addFromWorldFile("../../data/world.json");

    Player player = world.getPlayer();

    auto camera = world.getActiveCamera();
    GLFWwindow &window = _renderer.getWindow();
    Controller controller(window);
    player.setCamera(&camera);

    // init out fixed timestep
    const float deltaTime = 1.0 / 60; // target 60 FPS
    float accumulatedTime = 0.0;
    auto currentTime = chrono::high_resolution_clock::now();
    float timerSeconds = 90.0;

    createObjectiveList(world);

    while (!glfwWindowShouldClose(&window))
    {
        // calculate the rendering time from the last frame
        auto newTime = chrono::high_resolution_clock::now();
        chrono::duration<float> duration = newTime - currentTime;
        float frameTime = duration.count();
        currentTime = newTime;

        // add it to an accumulator
        // this is used to figure out how many steps we can run
        accumulatedTime += frameTime;

        // in-game timer
        if (_gameStarted && _validGame && !_objectiveCompleted)
            timerSeconds = fmax(timerSeconds - frameTime, 0);

        // consume our accumulated time one step at a time
        while (accumulatedTime >= deltaTime && _validGame && !_objectiveCompleted)
        {
            camera = world.getActiveCamera();

            // handle inputs

            if (Controller::_leftMouseButtonPressed && Controller::_cameraTurning)
            {
                float dx = Controller::_currentCursorPosX - Controller::_lastCursorPosX;
                float dy = Controller::_currentCursorPosY - Controller::_lastCursorPosY;

                camera.turn(dx, dy, deltaTime);

                Controller::_lastCursorPosX = Controller::_currentCursorPosX;
                Controller::_lastCursorPosY = Controller::_currentCursorPosY;

                if (camera.getCameraType() == CameraType::LOOK_AT)
                    player.turn();

                Controller::_cameraTurning = false;
            }

            if (Controller::_startGame && !_gameStarted)
            {
                world.setActiveCameraById("player lookat camera");
                _gameStarted = true;
                _renderer.hideStartScreen();
            }

            _renderer.toggleObjectiveList(Controller::_showObjective);

            if (camera.getCameraType() == CameraType::LOOK_AT)
            {
                if (Controller::_movingLeft && !Controller::_movingRight)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingForward || Controller::_movingBackward)
                        deltaMovement /= 1.5;

                    player.setOrientation(90);
                    auto displacement = camera.getRightVector() * -1.0f;
                    player.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }

                if (Controller::_movingRight && !Controller::_movingLeft)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingForward || Controller::_movingBackward)
                        deltaMovement /= 1.5;

                    player.setOrientation(-90);
                    auto displacement = camera.getRightVector();
                    player.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }

                if (Controller::_movingForward && !Controller::_movingBackward)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingLeft || Controller::_movingRight)
                        deltaMovement /= 1.5;

                    player.setOrientation(0);
                    auto displacement = camera.getViewVector();
                    player.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }

                if (Controller::_movingBackward && !Controller::_movingForward)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingLeft || Controller::_movingRight)
                        deltaMovement /= 1.5;

                    player.setOrientation(180);
                    auto displacement = camera.getViewVector() * -1.0f;
                    player.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }
            }
            else if (camera.getCameraType() == CameraType::FREE_DEBUG)
            {
                if (Controller::_movingForward && !Controller::_movingBackward)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingLeft || Controller::_movingRight)
                        deltaMovement /= 1.5;

                    auto displacement = camera.getViewVector();
                    camera.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }

                if (Controller::_movingBackward && !Controller::_movingForward)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingLeft || Controller::_movingRight)
                        deltaMovement /= 1.5;

                    auto displacement = camera.getViewVector() * -1.0f;
                    camera.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }

                if (Controller::_movingLeft && !Controller::_movingRight)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingForward || Controller::_movingBackward)
                        deltaMovement /= 1.5;

                    auto displacement = camera.getRightVector() * -1.0f;
                    camera.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }

                if (Controller::_movingRight && !Controller::_movingLeft)
                {
                    float deltaMovement = 3.0;

                    if (Controller::_movingForward || Controller::_movingBackward)
                        deltaMovement /= 1.5;

                    auto displacement = camera.getRightVector();
                    camera.displace(displacement.x, displacement.y, displacement.z, deltaMovement, deltaTime);
                }
            }

            // handle collisions between the player and every other entity
            auto const &playerEntity = player.getEntity();
            auto entities = world.getEntities();
            for (auto entityIt = entities.begin(); entityIt != entities.end(); ++entityIt)
            {
                auto &entity = entityIt->second;
                if (entity.getId() == playerEntity.getId())
                    continue; // don't check collision against self

                if (entity.getId() == "floor" || entity.getId() == "ceiling")
                    continue; // player's height is fixed

                if (entity.getId() == "dinosaur")
                    continue; // that tail is so unfair

                if (entity.getId() == "grass")
                    continue; // no fun in getting stuck

                if (CollisionHelper::checkCollision(playerEntity, entity, world))
                {
                    if (entity.isCollectible())
                    {
                        // TODO: game over messages
                        if (entity.getId() == "cow statue" || entity.getId() == "dinosaur_bbox")
                        {
                            _validGame = false;
                            _renderer.setGameOverMessage("How could I possibly carry that through the door?");
                        }
                        else
                        {
                            auto currentPosition = entity.getAbsolutePosition();
                            entity.setAbsolutePosition(currentPosition.x, 100, currentPosition.z);
                        }

                        if (!isInObjectiveList(entity.getId()))
                            timerSeconds = fmax(timerSeconds - 15, 0);
                        else
                            _objectsCollected++;

                        player.addToInventory(entity.getId());
                    }
                    else
                    {
                        if (entity.getId() == "exit" && _objectsCollected >= 5)
                        {
                            if (player.isInInventory("angel"))
                            {
                                _validGame = false;
                                _renderer.setGameOverMessage("I think I took something I shouldn't have... I need a Doctor");
                            }
                            else
                            {
                                _objectiveCompleted = true;
                            }
                        }
                        CollisionHelper::processPlayerCollision(player, entity, world);
                    }

                    world.updateEntityById(entity.getId(), entity);
                }
            }

            // turn globe around
            auto globe = world.getEntityById("Hollow Globe");
            auto rotation = globe.getRotationDegreesY();
            rotation += frameTime * 10;
            globe.setRotationDegreesY(rotation);
            world.updateEntityById("Hollow Globe", globe);

            // current position is valid
            player.recordPositionAsValid();

            // update the player
            world.updatePlayer(player);

            // update the camera
            world.updateCameraById(camera.getId(), camera);

            // step finished
            accumulatedTime -= deltaTime;
        }

        if (timerSeconds <= 0)
        {
            _validGame = false;
            _renderer.setGameOverMessage("Well, the cops are already here. No way out.");
        }

        if ((!_validGame || _objectiveCompleted) && Controller::_resetGame)
        {
            restartGame();  
            world.resetWorld("../../data/world.json");
            timerSeconds = 90.0;
            createObjectiveList(world);
            player.emptyInventory();
        }

        // render new frame
        _renderer.render(world, static_cast<int>(timerSeconds), _objectiveList, _validGame, _objectiveCompleted);
    }
}

bool Game::isInObjectiveList(string objectId)
{
    int objectInstances = count(_objectiveList.begin(), _objectiveList.end(), objectId);
    return (objectInstances >= 1);
}

void Game::createObjectiveList(World world)
{
    srand (time(NULL));
    std::vector<std::string> collectibles;

    auto entities = world.getEntities();
    for (auto entityIt = entities.begin(); entityIt != entities.end(); ++entityIt)
    {
        auto &entity = entityIt->second;

        if (entity.isCollectible())
        {
            if (entity.getId() != "angel" &&
                entity.getId() != "dinosaur_bbox" &&
                entity.getId() != "cow statue" &&
                entity.getId() != "hammer")
            {
                collectibles.push_back(entity.getId());
            }
        }
    }

    random_shuffle(collectibles.begin(), collectibles.end());

    std::vector<std::string> objective(collectibles.begin(), collectibles.begin()+5);
    _objectiveList = objective;
}

void Game::restartGame()
{
    _gameStarted = false;
    _validGame = true;
    _objectiveCompleted = false;
    _renderer.toggleObjectiveList(Controller::_showObjective);
    _objectsCollected = 0;
    _renderer.showStartScreen();
}