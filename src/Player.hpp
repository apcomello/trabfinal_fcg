#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__

#include "Entity.hpp"
#include "Camera.hpp"
#include <string>
#include <vector>

class Player
{
private:
  Entity *_entity;
  Camera *_camera;
  float _angle;
  float _orientation;
  glm::vec4 _previousPosition;
  float _previousOrientation;
  std::vector<std::string> _inventory;
  glm::vec4 _frontVector;
  glm::vec4 _rightVector;

public:
  Player(float x = 0, float y = 0, float z = 0);

  Entity &getEntity() const;
  Camera &getCamera() const;
  float getOrientation() const;
  glm::vec4 getPreviousPosition() const;
  float getPreviousOrientation() const;
  glm::vec4 getFrontVector() const;
  glm::vec4 getRightVector() const;

  void setEntity(Entity *entity);
  void setCamera(Camera *camera);
  void setOrientation(float orientation);

  void addToInventory(std::string objectId);
  void emptyInventory();
  bool isInInventory(std::string objectId);

  void turn();
  void displace(float x, float y, float z, float deltaMovement, float deltaTime);
  void moveTo(float x, float y, float z);
  void recordPositionAsValid();
  void interact(); // TODO: parameters
};

#endif // __PLAYER_HPP__
