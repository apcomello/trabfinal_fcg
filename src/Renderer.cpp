#include <iostream>
#include <fstream>
#include "glad/glad.h"
#include "Renderer.hpp"
#include "ShaderHelper.hpp"
#include "TextureHelper.hpp"

using namespace std;

Renderer::Renderer(string gameName, int width, int height)
    : _screenWidth(width), _screenHeight(height)
{
    // init GLFW
    if (!glfwInit())
    {
        cerr << "ERROR: glfwInit() failed." << endl;
        exit(EXIT_FAILURE);
    }

    // register callback for rendering errors
    glfwSetErrorCallback(static_cast<GLFWerrorfun>(errorCallback));

    // setup window and context
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    _window = glfwCreateWindow(width, height, gameName.c_str(), NULL, NULL);
    if (!_window)
    {
        glfwTerminate();
        cerr << "ERROR: glfwCreateWindow() failed." << endl;
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(_window);

    // load openGL via GLFW
    gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress));

    // disable V-SYNC
    glfwSwapInterval(0);

    // print hw info
    printGLInfo();

    // load user-defined shaders
    loadShaderProgramsFromShaderCfgFile("../../data/shaders.json");

    // load user-defined textures
    loadTexturesFromTextureAtlasFile("../../data/textures.json");

    // load text renderer
    int textShaderProgram = _shaderPrograms.at("text");
    _textRenderer.textRendererInit(textShaderProgram);

    // use z-buffer test
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // use alpha blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // use multisampling for less aliasing
    glEnable(GL_MULTISAMPLE);

    _showObjectives = false;
    _showStartScreen = true;
    _gameOverMessage = "GAME OVER";
}

Renderer::~Renderer()
{
    for (auto &p : _shaderPrograms)
        glDeleteProgram(p.second);

    glfwTerminate();
}

void Renderer::printGLInfo() const
{
    const GLubyte *vendor = glGetString(GL_VENDOR);
    const GLubyte *renderer = glGetString(GL_RENDERER);
    const GLubyte *glversion = glGetString(GL_VERSION);
    const GLubyte *glslversion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    const char *glfwVersion = glfwGetVersionString();

    cout << "GPU vendor: " << vendor << endl;
    cout << "Renderer: " << renderer << endl;
    cout << "OpenGL version: " << glversion << endl;
    cout << "GLSL version: " << glslversion << endl;
    cout << "GLFW version: " << glfwVersion << endl;

    delete vendor;
    delete renderer;
    delete glversion;
    delete glslversion;
    delete glfwVersion;
}

void Renderer::loadShaderProgramsFromShaderCfgFile(string fileName)
{
    cout << "Reading JSON file \"" << fileName << "\"..." << endl;

    // read json file
    nlohmann::json j;
    ifstream file(fileName);
    file >> j;

    cout << "Read JSON file \"" << fileName << "\"." << endl;

    if (j.count("shaders") > 0)
    {
        // compile and add each shader
        for (unsigned i = 0; i < j.at("shaders").size(); ++i)
        {
            auto const &jsonShader = j.at("shaders").at(i);

            // compile the shader
            string basePath = "../../data/shader/";
            string vertexShaderFileName = basePath + jsonShader.at("vertex").get<string>();
            string fragmentShaderFileName = basePath + jsonShader.at("fragment").get<string>();
            GLuint shaderProgram = ShaderHelper::createGPUProgram(vertexShaderFileName, fragmentShaderFileName);

            // get the id
            string id = "shader " + to_string(i);
            if (jsonShader.count("id") > 0)
                id = jsonShader.at("id");

            // add the shader
            pair<string, GLuint> mappedShader(id, shaderProgram);
            _shaderPrograms.insert(mappedShader);

            // if active, set it
            if (j.count("active") > 0 && j.at("active") == id)
                setActiveShaderProgram(id);
        }
    }
}

void Renderer::loadTexturesFromTextureAtlasFile(string fileName)
{
    cout << "Reading JSON file \"" << fileName << "\"..." << endl;

    // read json file
    nlohmann::json j;
    ifstream file(fileName);
    file >> j;

    cout << "Read JSON file \"" << fileName << "\"." << endl;

    if (j.count("textures") > 0)
    {
        // load and add each texture
        for (unsigned i = 0; i < j.at("textures").size(); ++i)
        {
            auto const &jsonTexture = j.at("textures").at(i);

            // load the image
            string basePath = "../../data/texture/";
            string textureImageFileName = basePath + jsonTexture.at("file").get<string>();
            int textureHelperId = TextureHelper::loadTextureImage(textureImageFileName);

            // get the id
            string id = "texture " + to_string(i);
            if (jsonTexture.count("id") > 0)
                id = jsonTexture.at("id");

            // add the texture
            pair<string, int> mappedTexture(id, textureHelperId);
            _textureImages.insert(mappedTexture);
        }
    }
}

void Renderer::registerWorld(World &world)
{
    auto entities = world.getEntities();
    for (auto entityIt = entities.begin(); entityIt != entities.end(); ++entityIt)
    {
        auto entity = entityIt->second;
        auto parts = entity.getEntityParts();
        for (auto partIt = parts.begin(); partIt != parts.end(); ++partIt)
        {
            auto part = partIt->second;

            // create the model's vertex arrays
            GLuint VAO_id;
            glGenVertexArrays(1, &VAO_id);

            glBindVertexArray(VAO_id);

            // feed the model's topology
            GLuint VBO_topology_id;
            glGenBuffers(1, &VBO_topology_id);
            glBindBuffer(GL_ARRAY_BUFFER, VBO_topology_id);
            auto vertexData = part.getVertices1DArray();
            glBufferData(GL_ARRAY_BUFFER, vertexData.size() * sizeof(float), vertexData.data(), GL_STATIC_DRAW);
            glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            // feed the model's normals
            if (part.getVertexNormals1DArraySize() > 0)
            {
                GLuint VBO_normal_id;
                glGenBuffers(1, &VBO_normal_id);
                glBindBuffer(GL_ARRAY_BUFFER, VBO_normal_id);
                auto normalData = part.getVertexNormals1DArray();
                glBufferData(GL_ARRAY_BUFFER, normalData.size() * sizeof(float), normalData.data(), GL_STATIC_DRAW);
                glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
                glEnableVertexAttribArray(1);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
            }

            // feed the model's texture UVs
            if (part.getVertexUVs1DArraySize() > 0)
            {
                GLuint VBO_UV_id;
                glGenBuffers(1, &VBO_UV_id);
                glBindBuffer(GL_ARRAY_BUFFER, VBO_UV_id);
                auto uvData = part.getVertexUVs1DArray();
                glBufferData(GL_ARRAY_BUFFER, uvData.size() * sizeof(float), uvData.data(), GL_STATIC_DRAW);
                glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
                glEnableVertexAttribArray(2);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
            }

            // feed the model's geometry
            GLuint indices_id;
            glGenBuffers(1, &indices_id);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_id);
            auto triangleData = part.getTriangles1DArray();
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangleData.size() * sizeof(int), triangleData.data(), GL_STATIC_DRAW);

            glBindVertexArray(0);

            world.setRendererId(entity.getId(), part.getId(), "vertex array id", VAO_id);
        }
    }

    world.setRegisteredByRenderer(true);
}

void Renderer::render(World &world, int counter, std::vector<std::string> objectiveList, bool validGame, bool objectiveCompleted)
{
    // validate world
    Camera cam = world.getActiveCamera();
    if (cam.getId() == "None")
        cerr << "WARNING: Attempting to render with no active camera" << endl;

    // guarantee info sent to openGL
    if (!world.getRegisteredByRenderer())
        registerWorld(world);

    // clear frame
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // black
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // light position
    auto const &playerEntity = world.getPlayer().getEntity();
    auto const &playerTorchPart = playerEntity.getEntityPartById("player torch top");
    auto torchRelativePosition = playerTorchPart.getRelativePosition();
    auto lightPosition = playerEntity.getAbsoluteTransform() * torchRelativePosition;
    lightPosition.y -= 0.01; // just a bit inside the torch

    // render each object in the world
    auto entities = world.getEntities();
    for (auto entityIt = entities.begin(); entityIt != entities.end(); ++entityIt)
    {
        auto entity = entityIt->second;
        renderEntity(world, cam, entity, lightPosition);
    }

    // render overlays
    float charwidth = _textRenderer.getCharWidth(_screenWidth);
    float lineheight = _textRenderer.getLineHeight(_screenHeight);

    if (validGame)
    {
        if (objectiveCompleted)
        {
            _textRenderer.printString("YOU WIN", 0.0 - 12*charwidth, 0.05, _screenWidth, _screenHeight, 3);
            _textRenderer.printString("But did you really?", 0 - 10 * charwidth, 0.0 - lineheight, _screenWidth, _screenHeight);
            _textRenderer.printString("Press SPACE if you want to start another game", 0 - 22 * charwidth, 0.0 - 3 * lineheight, _screenWidth, _screenHeight);
        }

        if (_showObjectives)
        {
            auto player = world.getPlayer();
            _textRenderer.printString("Objective", -0.9f, 0.9f, _screenWidth, _screenHeight, 1.5);
            for (auto object = objectiveList.begin(); object != objectiveList.end(); ++object)
            {
                auto const objectName = *object;
                auto index = object - objectiveList.begin();

                char formatedObject[50];
                if (player.isInInventory(objectName))
                    snprintf(formatedObject, 50, "[OK]  %s", objectName.c_str());
                else
                    snprintf(formatedObject, 50, "[  ]  %s", objectName.c_str());
                _textRenderer.printString(formatedObject, -0.9f, 0.8f - 0.05 * index, _screenWidth, _screenHeight, 1.5);
            }
        }

        if (_showStartScreen)
            _textRenderer.printStartScreen(_screenWidth, _screenHeight);

        _textRenderer.printTimer(counter, 1.0 - 15*charwidth, 1.0 - 3*lineheight, _screenWidth, _screenHeight, 2);

    }
    else
    {
        // TODO: Actual game over screen
        _textRenderer.printString("GAME OVER", 0 - (12*charwidth), 0.05, _screenWidth, _screenHeight, 3);
        _textRenderer.printString(_gameOverMessage, 0 - (_gameOverMessage.length()/2) * charwidth, 0.0 - lineheight, _screenWidth, _screenHeight);
        _textRenderer.printString("Press SPACE if you want to start another game", 0 - 22 * charwidth, 0.0 - 3 * lineheight, _screenWidth, _screenHeight);
    }
    // show new frame
    glfwSwapBuffers(_window);

    // process input
    glfwPollEvents();
}

void Renderer::renderEntity(World const &world, Camera const &camera, Entity const &entity, glm::vec4 lightPosition)
{
    // find the entity with the parts we want to render
    // we use a pointer because references aren't mutable after definition
    auto *sourceEntity = &entity;
    while (sourceEntity->getCloneFromId() != "None")
        sourceEntity = &world.getEntityById(sourceEntity->getCloneFromId());

    // render each part
    auto parts = sourceEntity->getEntityParts();
    for (auto partIt = parts.begin(); partIt != parts.end(); ++partIt)
    {
        auto const &part = partIt->second;
        renderEntityPart(camera, entity, sourceEntity, part, lightPosition);
    }
}

void Renderer::renderEntityPart(Camera const &camera, Entity const &entity, Entity const *sourceEntity, EntityPart const &part, glm::vec4 lightPosition)
{
    // find the part with the geometry we want to render
    // we use a pointer because references aren't mutable after definition
    auto *sourceEntityPart = &part;
    while (sourceEntityPart->getCloneFromId() != "None")
        sourceEntityPart = &sourceEntity->getEntityPartById(sourceEntityPart->getCloneFromId());

    // set every shader property we may need
    if (_shaderPrograms.size() > 0)
    {
        auto programId = _activeShaderProgram;

        // set the shader's projection matrix
        auto projectionMatrix = camera.getProjectionMatrix();
        auto uniformProjection = glGetUniformLocation(programId, "u_projection");
        glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

        // set the shader's view matrix
        auto viewMatrix = camera.getViewMatrix();
        auto uniformView = glGetUniformLocation(programId, "u_view");
        glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(viewMatrix));

        // set the shader's model matrix
        auto entityModelMatrix = entity.getAbsoluteTransform();
        auto partModelMatrix = part.getRelativeTransform();
        auto modelMatrix = entityModelMatrix * partModelMatrix;
        auto uniformModel = glGetUniformLocation(programId, "u_model");
        glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(modelMatrix));

        // set the shader's part ambient color
        auto ambientColor = part.getAmbientColor();
        auto uniformAmbientColor = glGetUniformLocation(programId, "u_ambient_color");
        glUniform3fv(uniformAmbientColor, 1, glm::value_ptr(ambientColor));

        // set the shader's part diffuse color
        auto diffuseColor = part.getDiffuseColor();
        auto uniformDiffuseColor = glGetUniformLocation(programId, "u_diffuse_color");
        glUniform3fv(uniformDiffuseColor, 1, glm::value_ptr(diffuseColor));

        // set the shader's part specular color
        auto specularColor = part.getSpecularColor();
        auto uniformSpecularColor = glGetUniformLocation(programId, "u_specular_color");
        glUniform3fv(uniformSpecularColor, 1, glm::value_ptr(specularColor));

        // set the shader's part shininess
        auto shininess = part.getShininess();
        auto uniformShininess = glGetUniformLocation(programId, "u_shininess");
        glUniform1f(uniformShininess, shininess);

        // set the shader's part opacity
        auto opacity = part.getOpacity();
        auto uniformOpacity = glGetUniformLocation(programId, "u_opacity");
        glUniform1f(uniformOpacity, opacity);

        // set the shader's part texture bool
        auto hasTexture = part.hasTexture();
        auto uniformHasTexture = glGetUniformLocation(programId, "u_has_texture");
        glUniform1i(uniformHasTexture, hasTexture);

        // set the shader's part normal mapping bool
        auto isTextureNormalMap = part.isTextureNormalMap();
        auto uniformIsTextureNormalMap = glGetUniformLocation(programId, "u_is_texture_normal_map");
        glUniform1i(uniformIsTextureNormalMap, isTextureNormalMap);

        // set the shader's part uv texture bool
        auto hasUV = part.hasUV();
        auto uniformHasUV = glGetUniformLocation(programId, "u_has_uv");
        glUniform1i(uniformHasUV, hasUV);

        // set the shader's part texture id
        auto textureId = part.getTextureId();
        if (textureId != "None")
        {
            GLuint textureHelperId = _textureImages.at(textureId);
            auto uniformTextureId = glGetUniformLocation(programId, "u_texture_id");
            glUniform1i(uniformTextureId, textureHelperId);
        }

        // set the shader's part illumination type
        auto usePerVertexIllumination = part.hasPerVertexIllumination();
        auto uniformUseGouraud = glGetUniformLocation(programId, "u_use_gouraud");
        glUniform1i(uniformUseGouraud, usePerVertexIllumination);

        // set the shader's part min bbox in local coordinates
        auto bboxMin = sourceEntityPart->getBBoxMinNoTranform();
        auto uniformBBoxMin = glGetUniformLocation(programId, "u_bbox_min");
        glUniform4fv(uniformBBoxMin, 1, glm::value_ptr(bboxMin));

        // set the shader's part max bbox in local coordinates
        auto bboxMax = sourceEntityPart->getBBoxMaxNoTranform();
        auto uniformBBoxMax = glGetUniformLocation(programId, "u_bbox_max");
        glUniform4fv(uniformBBoxMax, 1, glm::value_ptr(bboxMax));

        // set the shader's player position
        auto uniformLightPosition = glGetUniformLocation(programId, "u_light_position");
        glUniform4fv(uniformLightPosition, 1, glm::value_ptr(lightPosition));
    }

    // draw the part
    glBindVertexArray(sourceEntityPart->getRendererId("vertex array id"));
    glDrawElements(GL_TRIANGLES, sourceEntityPart->getTriangles1DArraySize(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void Renderer::setActiveShaderProgram(string id)
{
    _activeShaderProgram = _shaderPrograms.at(id);
    glUseProgram(_activeShaderProgram);
}

void Renderer::errorCallback(int error, const char *description)
{
    cerr << "ERROR: GLFW: " << description << endl;
}

GLFWwindow &Renderer::getWindow()
{
    return *_window;
}

void Renderer::toggleObjectiveList(bool showObjective)
{
    _showObjectives = showObjective;
}

void Renderer::hideStartScreen()
{
    _showStartScreen = false;
}

void Renderer::showStartScreen()
{
    _showStartScreen = true;
}

void Renderer::setGameOverMessage(std::string gameOverMessage)
{
    _gameOverMessage = gameOverMessage;
}