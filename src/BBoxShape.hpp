#ifndef __BBOX_SHAPE_HPP__
#define __BBOX_SHAPE_HPP__

enum class BBoxShape
{
    CUBE,
    SPHERE,
    CYLINDER,
};

#endif // __BBOX_SHAPE_HPP__