#include <iostream>
#include "glm/ext.hpp"
#include "Matrix.hpp"

using namespace std;

glm::mat4 Matrix::matrix(
    float m00, float m01, float m02, float m03,
    float m10, float m11, float m12, float m13,
    float m20, float m21, float m22, float m23,
    float m30, float m31, float m32, float m33)
{
    return glm::mat4(
        m00, m10, m20, m30,
        m01, m11, m21, m31,
        m02, m12, m22, m32,
        m03, m13, m23, m33);
}

glm::mat4 Matrix::matrixIdentity()
{
    return matrix(
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
}

glm::mat4 Matrix::matrixTranslate(float tx, float ty, float tz)
{
    return matrix(
        1.0, 0.0, 0.0, tx ,
        0.0, 1.0, 0.0, ty ,
        0.0, 0.0, 1.0, tz ,
        0.0, 0.0, 0.0, 1.0);
}

glm::mat4 Matrix::matrixTranslate(glm::vec3 t)
{
    return matrixTranslate(t.x, t.y, t.z);
}

glm::mat4 Matrix::matrixScale(float sx, float sy, float sz)
{
    return matrix(
        sx , 0.0, 0.0, 0.0,
        0.0, sy , 0.0, 0.0,
        0.0, 0.0, sz , 0.0,
        0.0, 0.0, 0.0, 1.0);
}

glm::mat4 Matrix::matrixScale(glm::vec3 s)
{
    return matrixScale(s.x, s.y, s.z);
}

glm::mat4 Matrix::matrixRotateDegreesX(float degrees)
{
    float c = cos(glm::radians(degrees));
    float s = sin(glm::radians(degrees));

    return matrix(
        1.0, 0.0, 0.0, 0.0,
        0.0,  c , -s , 0.0,
        0.0,  s ,  c , 0.0,
        0.0, 0.0, 0.0, 1.0);
}

glm::mat4 Matrix::matrixRotateDegreesY(float degrees)
{
    float c = cos(glm::radians(degrees));
    float s = sin(glm::radians(degrees));

    return matrix(
         c , 0.0,  s , 0.0,
        0.0, 1.0, 0.0, 0.0,
        -s , 0.0,  c , 0.0,
        0.0, 0.0, 0.0, 1.0);
}

glm::mat4 Matrix::matrixRotateDegreesZ(float degrees)
{
    float c = cos(glm::radians(degrees));
    float s = sin(glm::radians(degrees));

    return matrix(
         c , -s , 0.0, 0.0,
         s ,  c , 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
}

glm::mat4 Matrix::matrixRotateDegrees(float degrees, glm::vec4 axis)
{
    float c = cos(glm::radians(degrees));
    float s = sin(glm::radians(degrees));

    glm::vec4 v = axis / norm(axis);

    float vx = v.x;
    float vy = v.y;
    float vz = v.z;

    return matrix(
        vx*vx*(1.0-c)+c   , vx*vy*(1.0-c)-vz*s, vx*vz*(1-c)+vy*s, 0.0,
        vx*vy*(1.0-c)+vz*s, vy*vy*(1.0-c)+c   , vy*vz*(1-c)-vx*s, 0.0,
        vx*vz*(1-c)-vy*s  , vy*vz*(1-c)+vx*s  , vz*vz*(1.0-c)+c , 0.0,
        0.0               , 0.0               , 0.0             , 1.0);
}

float Matrix::norm(glm::vec3 v)
{
    return norm(glm::vec4(v, 0.0));
}

float Matrix::norm(glm::vec4 v)
{
    float vx = v.x;
    float vy = v.y;
    float vz = v.z;

    return sqrt(vx*vx + vy*vy + vz*vz);
}

glm::vec3 Matrix::normalize(glm::vec3 v)
{
    return v / norm(v);
}

glm::vec4 Matrix::normalize(glm::vec4 v)
{
    return v / norm(v);
}

glm::vec3 Matrix::crossProduct(glm::vec3 u, glm::vec3 v)
{
    float u1 = u.x;
    float u2 = u.y;
    float u3 = u.z;
    float v1 = v.x;
    float v2 = v.y;
    float v3 = v.z;

    return glm::vec3(
        u2*v3 - u3*v2,
        u3*v1 - u1*v3,
        u1*v2 - u2*v1);
}

glm::vec4 Matrix::crossProduct(glm::vec4 u, glm::vec4 v)
{
    float u1 = u.x;
    float u2 = u.y;
    float u3 = u.z;
    float v1 = v.x;
    float v2 = v.y;
    float v3 = v.z;

    return glm::vec4(
        u2*v3 - u3*v2,
        u3*v1 - u1*v3,
        u1*v2 - u2*v1,
        0.0);
}

float Matrix::dotProduct(glm::vec4 u, glm::vec4 v)
{
    float u1 = u.x;
    float u2 = u.y;
    float u3 = u.z;
    float u4 = u.w;
    float v1 = v.x;
    float v2 = v.y;
    float v3 = v.z;
    float v4 = v.w;

    if (u4 != 0.0 || v4 != 0.0)
        cerr << "ERROR: Produto escalar não definido para pontos." << endl;

    return u1*v1 + u2*v2 + u3*v3;
}

glm::vec3 Matrix::elementwiseMin(glm::vec3 u, glm::vec3 v)
{
    return glm::vec3(
        min(u.x, v.x),
        min(u.y, v.y),
        min(u.z, v.z));
}

glm::vec4 Matrix::elementwiseMin(glm::vec4 u, glm::vec4 v)
{
    return glm::vec4(
        min(u.x, v.x),
        min(u.y, v.y),
        min(u.z, v.z),
        min(u.w, v.w));
}

glm::vec3 Matrix::elementwiseMax(glm::vec3 u, glm::vec3 v)
{
    return glm::vec3(
        max(u.x, v.x),
        max(u.y, v.y),
        max(u.z, v.z));
}

glm::vec4 Matrix::elementwiseMax(glm::vec4 u, glm::vec4 v)
{
    return glm::vec4(
        max(u.x, v.x),
        max(u.y, v.y),
        max(u.z, v.z),
        max(u.w, v.w));
}

glm::vec3 Matrix::elementwiseAbs(glm::vec3 v)
{
    return glm::vec3(
        abs(v.x),
        abs(v.y),
        abs(v.z));
}

glm::vec4 Matrix::elementwiseAbs(glm::vec4 v)
{
    return glm::vec4(
        abs(v.x),
        abs(v.y),
        abs(v.z),
        abs(v.w));
}

glm::vec3 Matrix::elementwiseClamp(glm::vec3 v, glm::vec3 low, glm::vec3 high)
{
    return glm::vec3(
        min(max(v.x, low.x), high.x),
        min(max(v.y, low.y), high.y),
        min(max(v.z, low.z), high.z));
}

glm::vec4 Matrix::elementwiseClamp(glm::vec4 v, glm::vec4 low, glm::vec4 high)
{
    return glm::vec4(
        min(max(v.x, low.x), high.x),
        min(max(v.y, low.y), high.y),
        min(max(v.z, low.z), high.z),
        min(max(v.w, low.w), high.w));
}

glm::mat4 Matrix::matrixCameraView(glm::vec4 position, glm::vec4 viewVector, glm::vec4 upVector)
{
    glm::vec4 w = -viewVector;
    glm::vec4 u = crossProduct(upVector, w);

    w = w / norm(w);
    u = u / norm(u);

    glm::vec4 v = crossProduct(w,u);

    glm::vec4 origin = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

    float ux = u.x;
    float uy = u.y;
    float uz = u.z;
    float vx = v.x;
    float vy = v.y;
    float vz = v.z;
    float wx = w.x;
    float wy = w.y;
    float wz = w.z;

    return matrix(
        ux , uy , uz , -dotProduct(u , position - origin),
        vx , vy , vz , -dotProduct(v , position - origin),
        wx , wy , wz , -dotProduct(w , position - origin),
        0.0, 0.0, 0.0, 1.0);
}

glm::mat4 Matrix::matrixOrthographic(float top, float aspectRatio, float nearPlane, float farPlane)
{
    float bottom = -top;
    float right = top * aspectRatio;
    float left = -right;

    return matrix(
        2.0/(right-left), 0.0             , 0.0                     , -(right+left)/(right-left),
        0.0             , 2.0/(top-bottom), 0.0                     , -(top+bottom)/(top-bottom),
        0.0             , 0.0             , 2.0/(farPlane-nearPlane), -(farPlane+nearPlane)/(farPlane-nearPlane),
        0.0             , 0.0             , 0.0                     , 1.0);
}

glm::mat4 Matrix::matrixPerspective(float fieldOfView, float aspectRatio, float nearPlane, float farPlane)
{
    float top = fabs(nearPlane) * tanf(fieldOfView / 2.0f);

    glm::mat4 P = matrix(
        nearPlane, 0.0      , 0.0               , 0.0 ,
        0.0      , nearPlane, 0.0               , 0.0 ,
        0.0      , 0.0      , nearPlane+farPlane, -farPlane*nearPlane,
        0.0      , 0.0      , 1.0               , 0.0);

    glm::mat4 M = matrixOrthographic(top, aspectRatio, nearPlane, farPlane);

    return -M*P;
}

void Matrix::printMatrix(glm::mat4 mat)
{
    // transpose in order to print row-major
    cout << glm::to_string(transpose(mat)) << endl;
}

void Matrix::printVector(glm::vec3 vec)
{
    cout << to_string(vec) << endl;
}

void Matrix::printVector(glm::vec4 vec)
{

    cout << to_string(vec) << endl;
}
