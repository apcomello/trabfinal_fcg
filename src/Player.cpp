#include "Player.hpp"
#include "Matrix.hpp"
#include <algorithm>

using namespace std;

Player::Player(float x, float y, float z)
{
    _entity = nullptr;
    _angle = 0;
    _orientation = 0;
    _previousPosition = glm::vec4(x, y, z, 1.0);
    _previousOrientation = 0;
    _frontVector = glm::vec4(x, y, z+2, 0.0);
    _rightVector = Matrix::crossProduct( _frontVector, glm::vec4(0.0, 1.0, 0.0, 0.0));
}

Entity &Player::getEntity() const
{
    return *_entity;
}

Camera &Player::getCamera() const
{
    return *_camera;
}

float Player::getOrientation() const
{
    return _orientation;
}

glm::vec4 Player::getPreviousPosition() const
{
    return _previousPosition;
}

float Player::getPreviousOrientation() const
{
    return _previousOrientation;
}

glm::vec4 Player::getFrontVector() const
{
    return _frontVector;
}

glm::vec4 Player::getRightVector() const
{
    return _rightVector;
}

void Player::setEntity(Entity *entity)
{
    _entity = entity;
}

void Player::setCamera(Camera *camera)
{
    _camera = camera;
}

void Player::setOrientation(float orientation)
{
    _orientation = orientation;
}

void Player::addToInventory(std::string objectId)
{
    _inventory.push_back(objectId);
}

void Player::emptyInventory()
{
    _inventory.clear();
}

bool Player::isInInventory(string objectId)
{
    int objectInstances = count(_inventory.begin(), _inventory.end(), objectId);
    return (objectInstances >= 1);
}

void Player::turn()
{
    auto viewVector = -_camera->getViewVector();
    _angle = glm::degrees(atan2(viewVector.x, viewVector.z));
    _entity->setRotationDegreesY(_angle);
}

void Player::displace(float x, float y, float z, float deltaMovement, float deltaTime)
{
    auto projectedDisplacement = glm::vec4(x, 0.0, z, 0.0);
    glm::vec4 position = _entity->getAbsolutePosition();
    _entity->setRotationDegreesY(_angle + _orientation);

    // Movement in the xz plane only
    projectedDisplacement = projectedDisplacement / Matrix::norm(projectedDisplacement);

    if (_camera->getCameraType() == CameraType::LOOK_AT)
        _camera->displace(projectedDisplacement.x, projectedDisplacement.y, projectedDisplacement.z, deltaMovement, deltaTime);

    position += projectedDisplacement * deltaMovement * deltaTime;
    _entity->setAbsolutePosition(position.x, position.y, position.z);
}

void Player::moveTo(float x, float y, float z)
{
    glm::vec4 position(x, y, z, 1.0);
    glm::vec4 currentPosition = _entity->getAbsolutePosition();
    glm::vec4 displacement = position - currentPosition;

    currentPosition.x = position.x;
    currentPosition.z = position.z;

    if (_camera->getCameraType() == CameraType::LOOK_AT)
        _camera->displace(displacement.x, displacement.y, displacement.z, 1.0, 1.0);

    _entity->setAbsolutePosition(currentPosition.x, currentPosition.y, currentPosition.z);
}

void Player::recordPositionAsValid()
{
    _previousPosition = _entity->getAbsolutePosition();
    _previousOrientation = _orientation;
}

void Player::interact()
{
    // TODO: code me
}
