#ifndef __PROJECTION_TYPE_HPP__
#define __PROJECTION_TYPE_HPP__

enum class ProjectionType
{
    PERSPECTIVE,
    ORTHOGRAPHIC,
};

#endif // __PROJECTION_TYPE_HPP__