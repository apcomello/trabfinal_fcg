#ifndef __ENTITY_HPP__
#define __ENTITY_HPP__

#include <map>
#include "EntityPart.hpp"
#include "BBoxShape.hpp"

class Entity
{
private:
  std::string _id;
  std::string _cloneFromId;
  glm::vec4 _modelTransformedBBoxMin;
  glm::vec4 _modelTransformedBBoxMax;
  BBoxShape _bboxShape;
  std::map<std::string, EntityPart> _parts;
  glm::vec4 _absolutePosition;
  float _scaleFactorX;
  float _scaleFactorY;
  float _scaleFactorZ;
  float _rotationDegreesX;
  float _rotationDegreesY;
  float _rotationDegreesZ;
  std::map<std::string, int> _rendererIds;
  bool _isCollectible;

public:
  Entity(std::string const &id, std::string const &cloneFromId = "None");

  std::string const &getId() const;
  std::string const &getCloneFromId() const;
  std::map<std::string, EntityPart> const &getEntityParts() const;
  EntityPart const &getEntityPartById(std::string const &id) const;
  glm::vec4 getBBoxMin() const;
  glm::vec4 getBBoxMinNoTranform() const;
  glm::vec4 getBBoxMax() const;
  glm::vec4 getBBoxMaxNoTranform() const;
  BBoxShape getBBoxShape() const;
  glm::vec4 getAbsolutePosition() const;
  float getScaleFactorX() const;
  float getScaleFactorY() const;
  float getScaleFactorZ() const;
  float getRotationDegreesX() const;
  float getRotationDegreesY() const;
  float getRotationDegreesZ() const;
  bool isCollectible() const;
  glm::mat4 getAbsoluteTransform() const;
  int getRendererId(std::string const &name) const;

  void setId(std::string const &id);
  void setCloneFromId(std::string const &cloneFromId);
  void addEntityPart(EntityPart part);
  void clearParts();
  void setBBoxShape(BBoxShape bboxShape);
  void setAbsolutePosition(float x, float y, float z);
  void setScaleFactorX(float scaleFactorX);
  void setScaleFactorY(float scaleFactorY);
  void setScaleFactorZ(float scaleFactorZ);
  void setRotationDegreesX(float rotationDegreesX);
  void setRotationDegreesY(float rotationDegreesY);
  void setRotationDegreesZ(float rotationDegreesZ);
  void setCollectibleProperty(bool isCollectible);
  void resetRotation();
  void setRendererId(std::string const &name, int value);
  void setRendererId(std::string const &entityPartId, std::string const &name, int value);
  void updateEntityPartById(std::string const &id, EntityPart const &newEntityPart);

  static Entity fromObjFile(std::string const &objFileName);
};

#endif // __ENTITY_HPP__
