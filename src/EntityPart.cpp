#include <iostream>
#include "EntityPart.hpp"
#include "Matrix.hpp"

using namespace std;

EntityPart::EntityPart(string const &id, string const &cloneFromId)
{
    _id = id;
    _cloneFromId = cloneFromId;
    _relativePosition = glm::vec4(0.0, 0.0, 0.0, 1.0);
    _scaleFactorX = 1.0;
    _scaleFactorY = 1.0;
    _scaleFactorZ = 1.0;
    _rotationDegreesX = 0.0;
    _rotationDegreesY = 0.0;
    _rotationDegreesZ = 0.0;
    _ambientColor = glm::vec3(0.0, 0.0, 0.0);
    _diffuseColor = glm::vec3(0.0, 0.0, 0.0);
    _specularColor = glm::vec3(0.0, 0.0, 0.0);
    _shininess = 1.0;
    _opacity = 1.0;
    _textureId = "None";
    _hasTexture = false;
    _isTextureNormalMap = false;
    _hasUV = false;
    _usePerVertexIllumination = false;
}

string const &EntityPart::getId() const
{
    return _id;
}

string const &EntityPart::getCloneFromId() const
{
    return _cloneFromId;
}

glm::vec3 EntityPart::getAmbientColor() const
{
    return _ambientColor;
}

glm::vec3 EntityPart::getDiffuseColor() const
{
    return _diffuseColor;
}

glm::vec3 EntityPart::getSpecularColor() const
{
    return _specularColor;
}

float EntityPart::getShininess() const
{
    return _shininess;
}

float EntityPart::getOpacity() const
{
    return _opacity;
}

glm::vec4 const &EntityPart::getVertex(int vertexNum) const
{
    return _vertices.at(vertexNum);
}

vector<glm::vec4> const &EntityPart::getVertices() const
{
    return _vertices;
}

vector<float> EntityPart::getVertices1DArray() const
{
    vector<float> arr;

    for (auto const &v : _vertices)
    {
        for (int i = 0; i < 4; ++i)
            arr.push_back(v[i]);
    }

    return arr;
}

int EntityPart::getVertices1DArraySize() const
{
    return _vertices.size() * 4 * sizeof(float);
}

glm::vec3 const &EntityPart::getTriangle(int triangleNum) const
{
    return _triangles.at(triangleNum);
}

vector<glm::vec3> const &EntityPart::getTriangles() const
{
    return _triangles;
}

vector<int> EntityPart::getTriangles1DArray() const
{
    vector<int> arr;

    for (auto const &t : _triangles)
    {
        for (int i = 0; i < 3; ++i)
            arr.push_back(t[i]);
    }

    return arr;
}

int EntityPart::getTriangles1DArraySize() const
{
    return _triangles.size() * 3 * sizeof(int);
}

glm::vec4 const &EntityPart::getVertexNormal(int vertexNum) const
{
    return _vertexNormals.at(vertexNum);
}

vector<glm::vec4> const &EntityPart::getVertexNormals() const
{
    return _vertexNormals;
}

vector<float> EntityPart::getVertexNormals1DArray() const
{
    vector<float> arr;

    for (auto const &n : _vertexNormals)
    {
        for (int i = 0; i < 4; ++i)
            arr.push_back(n[i]);
    }

    return arr;
}

int EntityPart::getVertexNormals1DArraySize() const
{
    return _vertexNormals.size() * 4 * sizeof(float);
}

pair<float, float> const &EntityPart::getVertexUVs(int vertexNum) const
{
    return _vertexUVs.at(vertexNum);
}

vector<pair<float, float>> const &EntityPart::getVerticesUVs() const
{
    return _vertexUVs;
}

vector<float> EntityPart::getVertexUVs1DArray() const
{
    vector<float> arr;

    for (auto const &uv : _vertexUVs)
    {
        arr.push_back(uv.first);
        arr.push_back(uv.second);
    }

    return arr;
}

int EntityPart::getVertexUVs1DArraySize() const
{
    return _vertexUVs.size() * 2 * sizeof(float);
}

glm::vec3 EntityPart::getBBoxMin() const
{
    auto transformMatrix = getRelativeTransform();
    auto transformedMin = transformMatrix * _bboxMinNoTransform;
    auto transformedMax = transformMatrix * _bboxMaxNoTransform;

    return Matrix::elementwiseMin(transformedMin, transformedMax);
}

glm::vec4 EntityPart::getBBoxMinNoTranform() const
{
    return _bboxMinNoTransform;
}

glm::vec3 EntityPart::getBBoxMax() const
{
    auto transformMatrix = getRelativeTransform();
    auto transformedMin = transformMatrix * _bboxMinNoTransform;
    auto transformedMax = transformMatrix * _bboxMaxNoTransform;

    return Matrix::elementwiseMax(transformedMin, transformedMax);
}

glm::vec4 EntityPart::getBBoxMaxNoTranform() const
{
    return _bboxMaxNoTransform;
}

glm::vec4 EntityPart::getRelativePosition() const
{
    return _relativePosition;
}

float EntityPart::getScaleFactorX() const
{
    return _scaleFactorX;
}

float EntityPart::getScaleFactorY() const
{
    return _scaleFactorY;
}

float EntityPart::getScaleFactorZ() const
{
    return _scaleFactorZ;
}

float EntityPart::getRotationDegreesX() const
{
    return _rotationDegreesX;
}

float EntityPart::getRotationDegreesY() const
{
    return _rotationDegreesY;
}

float EntityPart::getRotationDegreesZ() const
{
    return _rotationDegreesZ;
}

bool EntityPart::hasTexture() const
{
    return _hasTexture;
}

bool EntityPart::isTextureNormalMap() const
{
    return _isTextureNormalMap;
}

bool EntityPart::hasUV() const
{
    return _hasUV;
}

string const &EntityPart::getTextureId() const
{
    return _textureId;
}

bool EntityPart::hasPerVertexIllumination() const
{
    return _usePerVertexIllumination;
}

int EntityPart::getRendererId(string const &name) const
{
    if (_rendererIds.count(name) == 0)
        return -1;

    return _rendererIds.at(name);
}

glm::mat4 EntityPart::getRelativeTransform() const
{
    // same order as in Lab3 to retain compatibility
    return Matrix::matrixTranslate(_relativePosition)
           * Matrix::matrixRotateDegreesZ(_rotationDegreesZ)
           * Matrix::matrixRotateDegreesY(_rotationDegreesY)
           * Matrix::matrixRotateDegreesX(_rotationDegreesX)
           * Matrix::matrixScale(_scaleFactorX, _scaleFactorY, _scaleFactorZ);
}

void EntityPart::setId(string const &id)
{
    _id = id;
}

void EntityPart::setCloneFromId(string const &cloneFromId)
{
    _cloneFromId = cloneFromId;
}

void EntityPart::setAmbientColor(glm::vec3 ambientColor)
{
    _ambientColor = ambientColor;
}

void EntityPart::setDiffuseColor(glm::vec3 diffuseColor)
{
    _diffuseColor = diffuseColor;
}

void EntityPart::setSpecularColor(glm::vec3 specularColor)
{
    _specularColor = specularColor;
}

void EntityPart::setShininess(float shininess)
{
    _shininess = shininess;
}

void EntityPart::setOpacity(float opacity)
{
    _opacity = opacity;
}

void EntityPart::addVertex(float x, float y, float z)
{
    _vertices.push_back(glm::vec4(x, y, z, 1.0));

    // update bbox
    if (_vertices.size() == 1)
    {
        _bboxMinNoTransform = glm::vec4(x, y, z, 1.0);
        _bboxMaxNoTransform = glm::vec4(x, y, z, 1.0);
    }
    else
    {
        _bboxMinNoTransform.x = min(_bboxMinNoTransform.x, x);
        _bboxMinNoTransform.y = min(_bboxMinNoTransform.y, y);
        _bboxMinNoTransform.z = min(_bboxMinNoTransform.z, z);

        _bboxMaxNoTransform.x = max(_bboxMaxNoTransform.x, x);
        _bboxMaxNoTransform.y = max(_bboxMaxNoTransform.y, y);
        _bboxMaxNoTransform.z = max(_bboxMaxNoTransform.z, z);
    }
}

void EntityPart::addVertices(vector<glm::vec3> const &vertices)
{
    for (auto const &v : vertices)
        addVertex(v.x, v.y, v.z);
}

void EntityPart::setVertices(vector<glm::vec3> const &vertices)
{
    _vertices.clear();
    addVertices(vertices);
}

void EntityPart::addTriangle(int i1, int i2, int i3)
{
    _triangles.push_back(glm::vec3(i1, i2, i3));
}

void EntityPart::addTriangles(vector<glm::vec3> const &triangles)
{
    _triangles.insert(_triangles.end(), triangles.begin(), triangles.end());
}

void EntityPart::setTriangles(vector<glm::vec3> const &triangles)
{
    _triangles = triangles;
}

void EntityPart::addVertexNormal(float x, float y, float z)
{
    _vertexNormals.push_back(glm::vec4(x, y, z, 0.0));
}

void EntityPart::addVertexNormals(vector<glm::vec3> const &vertexNormals)
{
    for (auto const &n : vertexNormals)
        addVertex(n.x, n.y, n.z);
}

void EntityPart::setVertexNormals(vector<glm::vec3> const &vertexNormals)
{
    _vertexNormals.clear();
    addVertexNormals(vertexNormals);
}

void EntityPart::addVertexUV(float U, float V)
{
    _vertexUVs.push_back(make_pair(U, V));
    _hasUV = true;
}

void EntityPart::addVertexUVs(vector<pair<float, float>> const &vertexUVs)
{
    _vertexUVs.insert(_vertexUVs.end(), vertexUVs.begin(), vertexUVs.end());
    _hasUV = true;
}

void EntityPart::setVertexUVs(vector<pair<float, float>> const &vertexUVs)
{
    _vertexUVs.clear();
    addVertexUVs(vertexUVs);
}

void EntityPart::setRelativePosition(float x, float y, float z)
{
    _relativePosition = glm::vec4(x, y, z, 1.0f);
}

void EntityPart::setScaleFactorX(float scaleFactorX)
{
    _scaleFactorX = scaleFactorX;
}

void EntityPart::setScaleFactorY(float scaleFactorY)
{
    _scaleFactorY = scaleFactorY;
}

void EntityPart::setScaleFactorZ(float scaleFactorZ)
{
    _scaleFactorZ = scaleFactorZ;
}

void EntityPart::setRotationDegreesX(float rotationDegreesX)
{
    _rotationDegreesX = rotationDegreesX;
}

void EntityPart::setRotationDegreesY(float rotationDegreesY)
{
    _rotationDegreesY = rotationDegreesY;
}

void EntityPart::setRotationDegreesZ(float rotationDegreesZ)
{
    _rotationDegreesZ = rotationDegreesZ;
}

void EntityPart::resetRotation()
{
    setRotationDegreesX(0.0);
    setRotationDegreesY(0.0);
    setRotationDegreesZ(0.0);
}

void EntityPart::setTextureId(string const &textureId)
{
    _textureId = textureId;
    _hasTexture = true;
}

void EntityPart::setTextureAsNormalMap()
{
    _isTextureNormalMap = true;
}

void EntityPart::setPerVertexIllumination()
{
    _usePerVertexIllumination = true;
}

void EntityPart::setRendererId(string const &name, int value)
{
    _rendererIds[name] = value;
}
