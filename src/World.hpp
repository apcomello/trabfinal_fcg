#ifndef __WORLD_HPP__
#define __WORLD_HPP__

#include "json.hpp"
#include "Entity.hpp"
#include "Camera.hpp"
#include "Player.hpp"

class World
{
private:
  Player _player;
  std::map<std::string, Entity> _entities;
  std::map<std::string, Camera> _cameras;
  std::string _activeCameraId;
  bool _registeredByRenderer;

public:
  World();

  void addEntity(Entity entity);
  void addCamera(Camera camera);
  void addFromWorldFile(std::string const &worldFileName);
  void addPlayerFromWorldFile(nlohmann::json const &jsonContents);
  void addCamerasFromWorldFile(nlohmann::json const &jsonContents);
  void addEntitiesFromWorldFile(nlohmann::json const &jsonContents);
  void addEntityPartsFromWorldFile(Entity &entity, nlohmann::json const &jsonEntity);
  Player const &getPlayer() const;
  std::map<std::string, Entity> const &getEntities() const;
  Entity const &getEntityById(std::string const &id) const;
  Camera const &getCameraById(std::string const &id) const;
  Camera const &getActiveCamera() const;
  bool getRegisteredByRenderer() const;
  void setActiveCameraById(std::string const &id);
  void setRegisteredByRenderer(bool registered);
  void setRendererId(std::string const &entityId, std::string const &entityPartId, std::string const &name, int value);
  void updatePlayer(Player const &newPlayer);
  void updateCameraById(std::string id, Camera const &newCamera);
  void updateEntityById(std::string id, Entity const &newEntity);
  void clearEntities();
  void clearCameras();
  void clearWorld();
  void resetWorld(std::string const &worldFileName);
  void resetPlayer(nlohmann::json const &jsonContents);
  void resetCollectibleEntities(nlohmann::json const &jsonContents);
  void resetCameras(nlohmann::json const &jsonContents);
};

#endif // __WORLD_HPP__
