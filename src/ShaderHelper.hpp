#ifndef __SHADER_HELPER_HPP__
#define __SHADER_HELPER_HPP__

#include <string>
#include "glad/glad.h"

class ShaderHelper
{
public:
  static GLuint createGPUProgram(
      std::string const &vertexShaderFileName,
      std::string const &fragmentShaderFileName);

private:
  static GLuint loadVertexShader(std::string const &vertexShaderFileName);
  static GLuint loadFragmentShader(std::string const &fragmentShaderFileName);
  static void loadShader(std::string const &shaderFileName, GLuint shaderId);
};

#endif // __SHADER_HELPER_HPP__
