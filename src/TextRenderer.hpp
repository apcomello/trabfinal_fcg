#ifndef __TEXT_RENDERER_HPP__
#define __TEXT_RENDERER_HPP__

#include <string>
#include "glad/glad.h"
#include "GLFW/glfw3.h"

class TextRenderer
{
  public:
    TextRenderer();

  private:
    GLuint _textVAO;
    GLuint _textVBO;
    GLuint _shaderProgramId;
    GLuint _textureId;
    GLuint _textureHelperId;
    GLuint _samplerId;
    float _textScale;

  public:
    void textRendererInit(int shaderProgramId);
    void printString(const std::string &str, float x, float y, int screenWidth, int screenHeight, float scale = 1.0f);
    void printTimer(int timer, float x, float y, int screenWidth, int screenHeight, float scale = 1.0f);
    void printStartScreen(int screenWidth, int screenHeight);
    float getLineHeight(int screenHeight);
    float getCharWidth(int screenWidth);
};

#endif // __TEXT_RENDERER_HPP__
