#include <iostream>
#include "glad/glad.h"
#include "stb_image.h"
#include "TextureHelper.hpp"

using namespace std;

int TextureHelper::_nextFreeTextureId = 0;

int TextureHelper::allocateTextureHelperId()
{
    // increment the next free texture id
    _nextFreeTextureId += 1;

    // return this image's texture id
    return _nextFreeTextureId - 1;
}

int TextureHelper::loadTextureImage(string textureImageFileName)
{
    cout << "Loading image as texture \"" << textureImageFileName << "\"..." << endl;

    // read image file
    stbi_set_flip_vertically_on_load(true);
    int width;
    int height;
    int channels;
    unsigned char *data = stbi_load(textureImageFileName.c_str(), &width, &height, &channels, 3);

    if (data == NULL)
    {
        cerr << "ERROR: Cannot open image file \"" << textureImageFileName << "\"." << endl;
        exit(EXIT_FAILURE);
    }

    cout << "Loaded image as texture \"" << textureImageFileName << "\"." << endl;
    cout << "Dimensions: " << width << "x" << height << endl;

    // allocate openGL texture and sampler
    GLuint textureId;
    GLuint samplerId;
    GLuint textureHelperId = allocateTextureHelperId();
    glGenTextures(1, &textureId);
    glGenSamplers(1, &samplerId);

    // set wrapping, mipmapping and unpacking
    glSamplerParameteri(samplerId, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(samplerId, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(samplerId, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(samplerId, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

    // bind texture and sampler
    glActiveTexture(GL_TEXTURE0 + textureHelperId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindSampler(textureHelperId, samplerId);

    // free the image
    stbi_image_free(data);

    return textureHelperId;
}
