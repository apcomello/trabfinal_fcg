#ifndef __TEXTURE_HELPER_HPP__
#define __TEXTURE_HELPER_HPP__

#include <string>

class TextureHelper
{
public:
  static int _nextFreeTextureId;

  static int allocateTextureHelperId();
  static int loadTextureImage(std::string textureImageFileName);
};

#endif // __TEXTURE_HELPER_HPP__
