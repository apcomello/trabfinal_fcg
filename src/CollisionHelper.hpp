#ifndef __COLLISION_HELPER_HPP__
#define __COLLISION_HELPER_HPP__

#include "World.hpp"

class CollisionHelper
{
  public:
    static bool checkCollision(Entity const &first, Entity const &second, World const &world);
    static void processPlayerCollision(Player &player, Entity &entity, World const &world);

  private:
    static bool checkCubeCubeCollision(Entity const &first, Entity const &second, Entity const &sourceFirst, Entity const &sourceSecond);
    static bool checkCubeSphereCollision(Entity const &cube, Entity const &sphere, Entity const &sourceCube, Entity const &sourceSphere);
    static bool checkCubeCylinderCollision(Entity const &cube, Entity const &cylinder, Entity const &sourceCube, Entity const &sourceCylinder);
    static void processPlayerCubeCollision(Player &player, Entity &cube, Entity const &sourceCube, World const &world);
    static void processPlayerSphereCollision(Player &player, Entity &sphere, Entity const &sourceSphere, World const &world);
    static void processPlayerCylinderCollision(Player &player, Entity &cylinder, Entity const &sourceCylinder, World const &world);
};

#endif // __COLLISION_HELPER_HPP__
