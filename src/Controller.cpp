#include "Controller.hpp"

// Static variables
bool Controller::_movingLeft = false;
bool Controller::_movingRight = false;
bool Controller::_movingForward = false;
bool Controller::_movingBackward = false;
bool Controller::_leftMouseButtonPressed = false;
bool Controller::_cameraTurning = false;
bool Controller::_showObjective = false;
bool Controller::_startGame = false;
bool Controller::_resetGame = false;

double Controller::_lastCursorPosX = 0.0;
double Controller::_lastCursorPosY = 0.0;
double Controller::_currentCursorPosX = 0.0;
double Controller::_currentCursorPosY = 0.0;

Controller::Controller(GLFWwindow &window)
{
    // Set all input callbacks
    glfwSetKeyCallback(&window, KeyCallback);
    glfwSetMouseButtonCallback(&window, MouseButtonCallback);
    glfwSetCursorPosCallback(&window, CursorPosCallback);
    glfwSetScrollCallback(&window, ScrollCallback);
}

void Controller::CursorPosCallback(GLFWwindow *window, double xpos, double ypos)
{
    if (!_leftMouseButtonPressed)
        return;

    _cameraTurning = true;
    _currentCursorPosX = xpos;
    _currentCursorPosY = ypos;
}

void Controller::KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mode)
{

    if (key == GLFW_KEY_A)
    {
        if (action == GLFW_PRESS)
            _movingLeft = true;
        else if (action == GLFW_RELEASE)
            _movingLeft = false;
    }

    if (key == GLFW_KEY_D)
    {
        if (action == GLFW_PRESS)
            _movingRight = true;
        else if (action == GLFW_RELEASE)
            _movingRight = false;
    }

    if (key == GLFW_KEY_W)
    {
        if (action == GLFW_PRESS)
            _movingForward = true;
        else if (action == GLFW_RELEASE)
            _movingForward = false;
    }

    if (key == GLFW_KEY_S)
    {
        if (action == GLFW_PRESS)
            _movingBackward = true;
        else if (action == GLFW_RELEASE)
            _movingBackward = false;
    }

    if (key == GLFW_KEY_TAB)
    {
        if (action == GLFW_PRESS)
            _showObjective = true;
        else if (action == GLFW_RELEASE)
            _showObjective = false;
    }

    if (key == GLFW_KEY_ENTER)
    {
        if (action == GLFW_PRESS)
        {
            _startGame = true;
        }
        else if (action == GLFW_RELEASE)
        {
            _startGame = false;
        }
    }

    if (key == GLFW_KEY_SPACE)
    {
        if (action == GLFW_PRESS)
        {
            _resetGame = true;
        }
        else if (action == GLFW_RELEASE)
        {
            _resetGame = false;
        }
    }

}

void Controller::MouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        if (action == GLFW_PRESS)
        {
            glfwGetCursorPos(window, &_lastCursorPosX, &_lastCursorPosY);
            _leftMouseButtonPressed = true;
        }
        else if (action == GLFW_RELEASE)
        {
            _leftMouseButtonPressed = false;
            _cameraTurning = false;
        }
    }
}

void Controller::ScrollCallback(GLFWwindow *window, double xoffset, double yoffset)
{
}
