#include <iostream>
#include "glm/ext.hpp"
#include "CollisionHelper.hpp"
#include "Matrix.hpp"

using namespace std;

bool CollisionHelper::checkCollision(Entity const &first, Entity const &second, World const &world)
{
    // find the original bbox shapes, in case of cloning
    // we use a pointer because references aren't mutable after definition
    auto *sourceFirst = &first;
    while (sourceFirst->getCloneFromId() != "None")
        sourceFirst = &world.getEntityById(sourceFirst->getCloneFromId());

    auto *sourceSecond = &second;
    while (sourceSecond->getCloneFromId() != "None")
        sourceSecond = &world.getEntityById(sourceSecond->getCloneFromId());

    auto firstBBoxShape = sourceFirst->getBBoxShape();
    auto secondBBoxShape = sourceSecond->getBBoxShape();

    if (firstBBoxShape != BBoxShape::CUBE && secondBBoxShape != BBoxShape::CUBE)
    {
        cerr << "No known collision combination for " << first.getId() << " and " << second.getId() << endl;
        return false;
    }

    if (firstBBoxShape == BBoxShape::CUBE && secondBBoxShape == BBoxShape::CUBE)
        return checkCubeCubeCollision(first, second, *sourceFirst, *sourceSecond);

    // below we know at least one is a cube, so figure out the non-cube

    if (firstBBoxShape == BBoxShape::CUBE && secondBBoxShape == BBoxShape::SPHERE)
        return checkCubeSphereCollision(first, second, *sourceFirst, *sourceSecond);

    if (firstBBoxShape == BBoxShape::SPHERE && secondBBoxShape == BBoxShape::CUBE)
        return checkCubeSphereCollision(second, first, *sourceSecond, *sourceFirst);

    if (firstBBoxShape == BBoxShape::CUBE && secondBBoxShape == BBoxShape::CYLINDER)
        return checkCubeCylinderCollision(first, second, *sourceFirst, *sourceSecond);

    if (firstBBoxShape == BBoxShape::CYLINDER && secondBBoxShape == BBoxShape::CUBE)
        return checkCubeCylinderCollision(second, first, *sourceSecond, *sourceFirst);

    // it's a cube and a shape not implemented
    cerr << "No known collision combination for " << first.getId() << " and " << second.getId() << endl;
    return false;
}

bool CollisionHelper::checkCubeCubeCollision(Entity const &first, Entity const &second, Entity const &sourceFirst, Entity const &sourceSecond)
{
    // get transformed source bboxes in case of entity cloning
    auto firstBBoxTransformedMin = first.getAbsoluteTransform() * sourceFirst.getBBoxMinNoTranform();
    auto firstBBoxTransformedMax = first.getAbsoluteTransform() * sourceFirst.getBBoxMaxNoTranform();
    auto firstBBoxMin = Matrix::elementwiseMin(firstBBoxTransformedMin, firstBBoxTransformedMax);
    auto firstBBoxMax = Matrix::elementwiseMax(firstBBoxTransformedMin, firstBBoxTransformedMax);
    auto secondBBoxTransformedMin = second.getAbsoluteTransform() * sourceSecond.getBBoxMinNoTranform();
    auto secondBBoxTransformedMax = second.getAbsoluteTransform() * sourceSecond.getBBoxMaxNoTranform();
    auto secondBBoxMin = Matrix::elementwiseMin(secondBBoxTransformedMin, secondBBoxTransformedMax);
    auto secondBBoxMax = Matrix::elementwiseMax(secondBBoxTransformedMin, secondBBoxTransformedMax);

    // distance between bbox centers
    auto firstCenter = (firstBBoxMin + firstBBoxMax) / 2.0;
    auto secondCenter = (secondBBoxMin + secondBBoxMax) / 2.0;
    auto centersDistances = Matrix::elementwiseAbs(firstCenter - secondCenter);

    // bboxes in half-sizes
    auto firstBBoxHalfSizes = abs(firstBBoxMax - firstBBoxMin) / 2.0;
    auto secondBBoxHalfSizes = abs(secondBBoxMax - secondBBoxMin) / 2.0;

    // check bbox intersection on all three axes
    if (centersDistances.x < firstBBoxHalfSizes.x + secondBBoxHalfSizes.x
        && centersDistances.y < firstBBoxHalfSizes.y + secondBBoxHalfSizes.y
        && centersDistances.z < firstBBoxHalfSizes.z + secondBBoxHalfSizes.z)
    {
        return true;
    }

    return false;
}

bool CollisionHelper::checkCubeSphereCollision(Entity const &cube, Entity const &sphere, Entity const &sourceCube, Entity const &sourceSphere)
{
    // get transformed source bboxes in case of entity cloning
    auto cubeBBoxTransformedMin = cube.getAbsoluteTransform() * sourceCube.getBBoxMinNoTranform();
    auto cubeBBoxTransformedMax = cube.getAbsoluteTransform() * sourceCube.getBBoxMaxNoTranform();
    auto cubeBBoxMin = Matrix::elementwiseMin(cubeBBoxTransformedMin, cubeBBoxTransformedMax);
    auto cubeBBoxMax = Matrix::elementwiseMax(cubeBBoxTransformedMin, cubeBBoxTransformedMax);
    auto sphereBBoxTransformedMin = sphere.getAbsoluteTransform() * sourceSphere.getBBoxMinNoTranform();
    auto sphereBBoxTransformedMax = sphere.getAbsoluteTransform() * sourceSphere.getBBoxMaxNoTranform();
    auto sphereBBoxMin = Matrix::elementwiseMin(sphereBBoxTransformedMin, sphereBBoxTransformedMax);
    auto sphereBBoxMax = Matrix::elementwiseMax(sphereBBoxTransformedMin, sphereBBoxTransformedMax);

    // radius of the sphere is the greatest axis distance
    auto sphereBBoxCenter = (sphereBBoxMin + sphereBBoxMax) / 2.0;
    auto sphereLargestDistanceBBoxMin = glm::compMax(Matrix::elementwiseAbs(sphereBBoxCenter - sphereBBoxMin));
    auto sphereLargestDistanceBBoxMax = glm::compMax(Matrix::elementwiseAbs(sphereBBoxCenter - sphereBBoxMax));
    auto sphereBBoxRadius = fmax(sphereLargestDistanceBBoxMin, sphereLargestDistanceBBoxMax);

    // smallest distance between the cube and the sphere (squared)
    auto cubeClosest = Matrix::elementwiseClamp(sphereBBoxCenter, cubeBBoxMin, cubeBBoxMax);
    auto sphereDistanceSquared = glm::distance2(cubeClosest, sphereBBoxCenter);

    if (sphereDistanceSquared <= sphereBBoxRadius * sphereBBoxRadius)
        return true;

    return false;
}

bool CollisionHelper::checkCubeCylinderCollision(Entity const &cube, Entity const &cylinder, Entity const &sourceCube, Entity const &sourceCylinder)
{
    // get transformed source bboxes in case of entity cloning
    auto cubeBBoxTransformedMin = cube.getAbsoluteTransform() * sourceCube.getBBoxMinNoTranform();
    auto cubeBBoxTransformedMax = cube.getAbsoluteTransform() * sourceCube.getBBoxMaxNoTranform();
    auto cubeBBoxMin = Matrix::elementwiseMin(cubeBBoxTransformedMin, cubeBBoxTransformedMax);
    auto cubeBBoxMax = Matrix::elementwiseMax(cubeBBoxTransformedMin, cubeBBoxTransformedMax);
    auto cylinderBBoxTransformedMin = cylinder.getAbsoluteTransform() * sourceCylinder.getBBoxMinNoTranform();
    auto cylinderBBoxTransformedMax = cylinder.getAbsoluteTransform() * sourceCylinder.getBBoxMaxNoTranform();
    auto cylinderBBoxMin = Matrix::elementwiseMin(cylinderBBoxTransformedMin, cylinderBBoxTransformedMax);
    auto cylinderBBoxMax = Matrix::elementwiseMax(cylinderBBoxTransformedMin, cylinderBBoxTransformedMax);

    // distance between bbox centers
    auto cubeBBoxCenter = (cubeBBoxMin + cubeBBoxMax) / 2.0;
    auto cylinderBBoxCenter = (cylinderBBoxMin + cylinderBBoxMax) / 2.0;
    auto centersDistances = Matrix::elementwiseAbs(cubeBBoxCenter - cylinderBBoxCenter);

    // bboxes in half-sizes
    auto cubeBBoxHalfSizes = abs(cubeBBoxMax - cubeBBoxMin) / 2.0;
    auto cylinderBBoxHalfSizes = abs(cylinderBBoxMax - cylinderBBoxMin) / 2.0;

    // must intersect on the y axis
    if (centersDistances.y > cubeBBoxHalfSizes.y + cylinderBBoxHalfSizes.y)
        return false;

    // below, test collision between 2D rectangle and circle, both projected on the xz plane
    auto rectangleBBoxMin = glm::vec4(cubeBBoxMin.x, 0.0, cubeBBoxMin.z, 1.0);
    auto rectangleBBoxMax = glm::vec4(cubeBBoxMax.x, 0.0, cubeBBoxMax.z, 1.0);
    auto circleBBoxCenter = glm::vec4(cylinderBBoxCenter.x, 0.0, cylinderBBoxCenter.z, 1.0);
    auto circleBBoxMin = glm::vec4(cylinderBBoxMin.x, 0.0, cylinderBBoxMin.z, 1.0);
    auto circleBBoxMax = glm::vec4(cylinderBBoxMax.x, 0.0, cylinderBBoxMax.z, 1.0);

    // radius of the xz circle
    auto circleLargestDistanceBBoxMin = glm::compMax(Matrix::elementwiseAbs(circleBBoxCenter - circleBBoxMin));
    auto circleLargestDistanceBBoxMax = glm::compMax(Matrix::elementwiseAbs(circleBBoxCenter - circleBBoxMax));
    auto circleBBoxRadius = fmax(circleLargestDistanceBBoxMin, circleLargestDistanceBBoxMax);

    // smallest distance between the circle and the rectangle (squared)
    auto rectangleClosest = Matrix::elementwiseClamp(circleBBoxCenter, rectangleBBoxMin, rectangleBBoxMax);
    auto circleDistanceSquared = glm::distance2(rectangleClosest, circleBBoxCenter);

    if (circleDistanceSquared <= circleBBoxRadius * circleBBoxRadius)
        return true;

    return false;
}

void CollisionHelper::processPlayerCollision(Player &player, Entity &entity, World const &world)
{
    // find the entity we want to check
    // we use a pointer because references aren't mutable after definition
    auto const *sourceEntity = &entity;
    while (sourceEntity->getCloneFromId() != "None")
        sourceEntity = &world.getEntityById(sourceEntity->getCloneFromId());

    auto entityBBoxShape = sourceEntity->getBBoxShape();

    if (entityBBoxShape == BBoxShape::CUBE)
        processPlayerCubeCollision(player, entity, *sourceEntity, world);
    else if (entityBBoxShape == BBoxShape::SPHERE)
        processPlayerSphereCollision(player, entity, *sourceEntity, world);
    else if (entityBBoxShape == BBoxShape::CYLINDER)
        processPlayerCylinderCollision(player, entity, *sourceEntity, world);
    else
        cerr << "No known collision response for player and " << entity.getId() << endl;
}

void CollisionHelper::processPlayerCubeCollision(Player &player, Entity &cube, Entity const &sourceCube, World const &world)
{
    auto &playerEntity = player.getEntity();
    auto playerPosition = playerEntity.getAbsolutePosition();
    auto playerOrientation = player.getOrientation();

    // try simply setting the previous position and orientation
    auto lastValidPosition = player.getPreviousPosition();
    auto lastValidOrientation = player.getPreviousOrientation();
    if (playerPosition != lastValidPosition
        || lastValidOrientation != playerOrientation)
    {
        player.moveTo(lastValidPosition.x, lastValidPosition.y, lastValidPosition.z);
        player.setOrientation(lastValidOrientation);
        return;
    }

    // get transformed source bboxes in case of entity cloning
    auto cubeBBoxTransformedMin = cube.getAbsoluteTransform() * sourceCube.getBBoxMinNoTranform();
    auto cubeBBoxTransformedMax = cube.getAbsoluteTransform() * sourceCube.getBBoxMaxNoTranform();
    auto cubeBBoxMin = Matrix::elementwiseMin(cubeBBoxTransformedMin, cubeBBoxTransformedMax);
    auto cubeBBoxMax = Matrix::elementwiseMax(cubeBBoxTransformedMin, cubeBBoxTransformedMax);

    // point in the cube that's closest to the player's position
    auto playerBBoxCenter = (playerEntity.getBBoxMin() + playerEntity.getBBoxMax()) / 2.0;
    auto cubeClosest = Matrix::elementwiseClamp(playerBBoxCenter, cubeBBoxMin, cubeBBoxMax);

    // project both on the xz plane and create a 2D vector
    playerBBoxCenter.y = 0.0;
    cubeClosest.y = 0.0;
    auto displacement2D = playerBBoxCenter - cubeClosest;

    // move in the xz plane opposite to the vector
    // TODO: hardcoded delta?
    while (checkCollision(playerEntity, cube, world)){
        player.displace(displacement2D.x, displacement2D.y, displacement2D.z, 0.001, 1.0);
    }

    player.setEntity(&playerEntity);
}

void CollisionHelper::processPlayerSphereCollision(Player &player, Entity &sphere, Entity const &sourceSphere, World const &world)
{
    auto &playerEntity = player.getEntity();
    auto playerPosition = playerEntity.getAbsolutePosition();
    auto playerOrientation = player.getOrientation();

    // try simply setting the previous position and orientation
    auto lastValidPosition = player.getPreviousPosition();
    auto lastValidOrientation = player.getPreviousOrientation();
    if (playerPosition != lastValidPosition
        || lastValidOrientation != playerOrientation)
    {
        player.moveTo(lastValidPosition.x, lastValidPosition.y, lastValidPosition.z);
        player.setOrientation(lastValidOrientation);
        return;
    }

    // get transformed source bboxes in case of entity cloning
    auto sphereBBoxTransformedMin = sphere.getAbsoluteTransform() * sourceSphere.getBBoxMinNoTranform();
    auto sphereBBoxTransformedMax = sphere.getAbsoluteTransform() * sourceSphere.getBBoxMaxNoTranform();
    auto sphereBBoxMin = Matrix::elementwiseMin(sphereBBoxTransformedMin, sphereBBoxTransformedMax);
    auto sphereBBoxMax = Matrix::elementwiseMax(sphereBBoxTransformedMin, sphereBBoxTransformedMax);

    // get both bbox centers
    auto playerBBoxCenter = (playerEntity.getBBoxMin() + playerEntity.getBBoxMax()) / 2.0;
    auto sphereBBoxCenter = (sphereBBoxMin + sphereBBoxMax) / 2.0;

    // project both bbox centers on the xz plane and create a 2D vector
    playerBBoxCenter.y = 0.0;
    sphereBBoxCenter.y = 0.0;
    auto displacement2D = playerBBoxCenter - sphereBBoxCenter;

    // move in the xz plane opposite to the vector
    // TODO: hardcoded delta?
    while (checkCollision(playerEntity, sphere, world))
        player.displace(displacement2D.x, displacement2D.y, displacement2D.z, 0.001, 1.0);

    player.setEntity(&playerEntity);
}

void CollisionHelper::processPlayerCylinderCollision(Player &player, Entity &cylinder, Entity const &sourceCylinder, World const &world)
{
    // 2D response same as the sphere
    processPlayerSphereCollision(player, cylinder, sourceCylinder, world);
}
