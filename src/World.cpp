#include <iostream>
#include <fstream>
#include "World.hpp"

using namespace std;

World::World()
    : _activeCameraId("None")
{
    _registeredByRenderer = false;
}

void World::addEntity(Entity entity)
{
    if (_registeredByRenderer)
        cerr << "WARNING: entity added after renderer registration" << endl;

    if (_entities.count(entity.getId()) > 0)
        cerr << "WARNING: added entity already in use, overwriting" << endl;

    pair<string, Entity> mappedEntity(entity.getId(), entity);
    _entities.insert(mappedEntity);
}

void World::addCamera(Camera camera)
{
    if (_cameras.count(camera.getId()) > 0)
        cerr << "WARNING: added camera already in use, overwriting" << endl;

    pair<string, Camera> mappedCamera(camera.getId(), camera);
    _cameras.insert(mappedCamera);

    if (_activeCameraId == "None")
        _activeCameraId = camera.getId();
}

void World::addFromWorldFile(string const &worldFileName)
{
    if (_registeredByRenderer)
        cerr << "WARNING: world file loaded after renderer registration" << endl;

    cout << "Reading JSON file \"" << worldFileName << "\"..." << endl;

    // read json file
    nlohmann::json jsonContents;
    ifstream file(worldFileName);
    file >> jsonContents;

    cout << "Read JSON file \"" << worldFileName << "\"." << endl;

    // add everything in it to the world
    addPlayerFromWorldFile(jsonContents);
    addCamerasFromWorldFile(jsonContents);
    addEntitiesFromWorldFile(jsonContents);
}

void World::addPlayerFromWorldFile(nlohmann::json const &jsonContents)
{
    if (jsonContents.count("playerEntity") != 0)
    {
        cout << "Adding player from world file" << endl;

        auto jsonPlayer = jsonContents.at("playerEntity");

        Player player;

        Entity entity("Player");

        addEntityPartsFromWorldFile(entity, jsonPlayer);

        if (jsonPlayer.count("absolutePosition") != 0)
        {
            float x = jsonPlayer.at("absolutePosition")[0];
            float y = jsonPlayer.at("absolutePosition")[1];
            float z = jsonPlayer.at("absolutePosition")[2];
            entity.setAbsolutePosition(x, y, z); // TODO: should this be player.moveTo?
        }

        if (jsonPlayer.count("rotationDegreesZ") != 0)
            entity.setRotationDegreesZ(jsonPlayer.at("rotationDegreesZ"));

        if (jsonPlayer.count("rotationDegreesY") != 0)
            entity.setRotationDegreesY(jsonPlayer.at("rotationDegreesY"));

        if (jsonPlayer.count("rotationDegreesX") != 0)
            entity.setRotationDegreesX(jsonPlayer.at("rotationDegreesX"));

        addEntity(entity);

        Entity &mappedEntity = _entities.at(entity.getId());
        player.setEntity(&mappedEntity);

        _player = player;

        cout << "Added player from world file" << endl;
    }
}

void World::addCamerasFromWorldFile(nlohmann::json const &jsonContents)
{
    if (jsonContents.count("cameras") != 0)
    {
        cout << "Adding cameras from world file" << endl;

        // add each camera
        for (unsigned i = 0; i < jsonContents.at("cameras").size(); ++i)
        {
            auto jsonCam = jsonContents.at("cameras")[i];

            Camera cam("From file");

            if (jsonCam.count("id") != 0)
                cam.setId(jsonCam.at("id"));

            if (jsonCam.count("cameraType") != 0)
            {
                if (jsonCam.at("cameraType") == "LOOK_AT")
                    cam.setCameraType(CameraType::LOOK_AT);
                else if (jsonCam.at("cameraType") == "FREE_FIXED")
                    cam.setCameraType(CameraType::FREE_FIXED);
                else if (jsonCam.at("cameraType") == "FREE_DEBUG")
                    cam.setCameraType(CameraType::FREE_DEBUG);
            }

            if (jsonCam.count("projectionType") != 0)
            {
                if (jsonCam.at("projectionType") == "PERSPECTIVE")
                    cam.setProjectionType(ProjectionType::PERSPECTIVE);
                else if (jsonCam.at("projectionType") == "ORTHOGRAPHIC")
                    cam.setProjectionType(ProjectionType::ORTHOGRAPHIC);
            }

            if (jsonCam.count("fieldOfView") != 0)
                cam.setFieldOfView(jsonCam.at("fieldOfView"));

            if (jsonCam.count("nearPlane") != 0)
                cam.setNearPlane(jsonCam.at("nearPlane"));

            if (jsonCam.count("farPlane") != 0)
                cam.setFarPlane(jsonCam.at("farPlane"));

            if (jsonCam.count("aspectRatio") != 0)
                cam.setAspectRatio(jsonCam.at("aspectRatio"));

            if (jsonCam.count("position") != 0)
            {
                float x = jsonCam.at("position")[0];
                float y = jsonCam.at("position")[1];
                float z = jsonCam.at("position")[2];
                cam.setPosition(x, y, z);
            }

            if (jsonCam.count("lookAtPosition") != 0)
            {
                float x = jsonCam.at("lookAtPosition")[0];
                float y = jsonCam.at("lookAtPosition")[1];
                float z = jsonCam.at("lookAtPosition")[2];
                cam.setLookAtPosition(x, y, z);
                cam.setOriginalLookAtPosition(x, y, z);
            }

            if (jsonCam.count("viewVector") != 0)
            {
                float x = jsonCam.at("viewVector")[0];
                float y = jsonCam.at("viewVector")[1];
                float z = jsonCam.at("viewVector")[2];
                cam.setViewVector(x, y, z);
            }

            if (jsonCam.count("upVector") != 0)
            {
                float x = jsonCam.at("upVector")[0];
                float y = jsonCam.at("upVector")[1];
                float z = jsonCam.at("upVector")[2];
                cam.setUpVector(x, y, z);
            }

            addCamera(cam);

            if (jsonContents.count("activeCamera") && jsonContents.at("activeCamera") == i)
                setActiveCameraById(cam.getId());
        }

        cout << "Added cameras from world file" << endl;
    }
}

void World::addEntitiesFromWorldFile(nlohmann::json const &jsonContents)
{
    if (jsonContents.count("entities") != 0)
    {
        cout << "Adding entities from world file" << endl;

        // add each entity
        for (auto const &jsonEntity : jsonContents.at("entities"))
        {
            Entity entity("From file");

            if (jsonEntity.count("fromObjFile") != 0)
            {
                string basePath = "../../data/obj/";
                string objFileName = basePath + jsonEntity.at("fromObjFile").get<string>();
                entity = Entity::fromObjFile(objFileName);
            }

            if (jsonEntity.count("id") != 0)
                entity.setId(jsonEntity.at("id"));

            if (jsonEntity.count("cloneFromId") != 0)
                entity.setCloneFromId(jsonEntity.at("cloneFromId"));

            if (jsonEntity.count("parts") != 0)
                addEntityPartsFromWorldFile(entity, jsonEntity);

            if (jsonEntity.count("scaleFactorX") != 0)
                entity.setScaleFactorX(jsonEntity.at("scaleFactorX"));

            if (jsonEntity.count("scaleFactorY") != 0)
                entity.setScaleFactorY(jsonEntity.at("scaleFactorY"));

            if (jsonEntity.count("scaleFactorZ") != 0)
                entity.setScaleFactorZ(jsonEntity.at("scaleFactorZ"));

            if (jsonEntity.count("absolutePosition") != 0)
            {
                float x = jsonEntity.at("absolutePosition")[0];
                float y = jsonEntity.at("absolutePosition")[1];
                float z = jsonEntity.at("absolutePosition")[2];
                entity.setAbsolutePosition(x, y, z);
            }

            if (jsonEntity.count("isCollectible") != 0)
                entity.setCollectibleProperty(jsonEntity.at("isCollectible"));

            if (jsonEntity.count("rotationDegreesZ") != 0)
                entity.setRotationDegreesZ(jsonEntity.at("rotationDegreesZ"));

            if (jsonEntity.count("rotationDegreesY") != 0)
                entity.setRotationDegreesY(jsonEntity.at("rotationDegreesY"));

            if (jsonEntity.count("rotationDegreesX") != 0)
                entity.setRotationDegreesX(jsonEntity.at("rotationDegreesX"));

            if (jsonEntity.count("BBoxShape") != 0)
            {
                if (jsonEntity.at("BBoxShape") == "CUBE")
                    entity.setBBoxShape(BBoxShape::CUBE);
                else if (jsonEntity.at("BBoxShape") == "SPHERE")
                    entity.setBBoxShape(BBoxShape::SPHERE);
                else if (jsonEntity.at("BBoxShape") == "CYLINDER")
                    entity.setBBoxShape(BBoxShape::CYLINDER);
            }

            addEntity(entity);
        }

        cout << "Added entities from world file" << endl;
    }
}

void World::addEntityPartsFromWorldFile(Entity &entity, nlohmann::json const &jsonEntity)
{
    // add each entity part
    for (auto const &jsonEntityPart : jsonEntity.at("parts"))
    {
        EntityPart part("From file");

        bool updating = false;

        if (jsonEntityPart.count("id") != 0)
        {
            string id = jsonEntityPart.at("id");
            auto &existingParts = entity.getEntityParts();
            if (existingParts.count(id) > 0)
            {
                part = existingParts.at(id);
                updating = true;
            }
            else
            {
                part.setId(jsonEntityPart.at("id"));
            }
        }

        if (jsonEntityPart.count("cloneFromId") != 0)
            part.setCloneFromId(jsonEntityPart.at("cloneFromId"));

        if (jsonEntityPart.count("vertices") != 0)
        {
            // add each vertex
            for (auto const &jsonEntityPartVertices : jsonEntityPart.at("vertices"))
            {
                float x = jsonEntityPartVertices[0];
                float y = jsonEntityPartVertices[1];
                float z = jsonEntityPartVertices[2];
                part.addVertex(x, y, z);
            }
        }

        if (jsonEntityPart.count("triangles") != 0)
        {
            // add each triangle
            for (auto const &jsonEntityPartTriangles : jsonEntityPart.at("triangles"))
            {
                float i1 = jsonEntityPartTriangles[0];
                float i2 = jsonEntityPartTriangles[1];
                float i3 = jsonEntityPartTriangles[2];
                part.addTriangle(i1, i2, i3);
            }
        }

        if (jsonEntityPart.count("normals") != 0)
        {
            // add each normal
            for (auto const &jsonEntityPartNormals : jsonEntityPart.at("normals"))
            {
                float x = jsonEntityPartNormals[0];
                float y = jsonEntityPartNormals[1];
                float z = jsonEntityPartNormals[2];
                part.addVertexNormal(x, y, z);
            }
        }

        if (jsonEntityPart.count("relativePosition") != 0)
        {
            float x = jsonEntityPart.at("relativePosition")[0];
            float y = jsonEntityPart.at("relativePosition")[1];
            float z = jsonEntityPart.at("relativePosition")[2];
            part.setRelativePosition(x, y, z);
        }

        if (jsonEntityPart.count("colorAmbient") != 0)
        {
            float r = jsonEntityPart.at("colorAmbient")[0];
            float g = jsonEntityPart.at("colorAmbient")[1];
            float b = jsonEntityPart.at("colorAmbient")[2];
            part.setAmbientColor(glm::vec3(r, g, b));
        }

        if (jsonEntityPart.count("colorDiffuse") != 0)
        {
            float r = jsonEntityPart.at("colorDiffuse")[0];
            float g = jsonEntityPart.at("colorDiffuse")[1];
            float b = jsonEntityPart.at("colorDiffuse")[2];
            part.setDiffuseColor(glm::vec3(r, g, b));
        }

        if (jsonEntityPart.count("colorSpecular") != 0)
        {
            float r = jsonEntityPart.at("colorSpecular")[0];
            float g = jsonEntityPart.at("colorSpecular")[1];
            float b = jsonEntityPart.at("colorSpecular")[2];
            part.setSpecularColor(glm::vec3(r, g, b));
        }

        if (jsonEntityPart.count("shininess") != 0)
            part.setShininess(jsonEntityPart.at("shininess"));

        if (jsonEntityPart.count("opacity") != 0)
            part.setOpacity(jsonEntityPart.at("opacity"));

        if (jsonEntityPart.count("textureId") != 0)
            part.setTextureId(jsonEntityPart.at("textureId"));

        if (jsonEntityPart.count("isTextureNormalMap") != 0)
        {
            if (jsonEntityPart.at("isTextureNormalMap"))
                part.setTextureAsNormalMap();
        }

        if (jsonEntityPart.count("perVertexIllumination") != 0)
        {
            if (jsonEntityPart.at("perVertexIllumination"))
                part.setPerVertexIllumination();
        }

        if (jsonEntityPart.count("scaleFactorX") != 0)
            part.setScaleFactorX(jsonEntityPart.at("scaleFactorX"));

        if (jsonEntityPart.count("scaleFactorY") != 0)
            part.setScaleFactorY(jsonEntityPart.at("scaleFactorY"));

        if (jsonEntityPart.count("scaleFactorZ") != 0)
            part.setScaleFactorZ(jsonEntityPart.at("scaleFactorZ"));

        if (jsonEntityPart.count("rotationDegreesZ") != 0)
            part.setRotationDegreesZ(jsonEntityPart.at("rotationDegreesZ"));

        if (jsonEntityPart.count("rotationDegreesY") != 0)
            part.setRotationDegreesY(jsonEntityPart.at("rotationDegreesY"));

        if (jsonEntityPart.count("rotationDegreesX") != 0)
            part.setRotationDegreesX(jsonEntityPart.at("rotationDegreesX"));

        if (updating)
            entity.updateEntityPartById(jsonEntityPart.at("id"), part);
        else
            entity.addEntityPart(part);
    }
}

Player const &World::getPlayer() const
{
    return _player;
}

map<string, Entity> const &World::getEntities() const
{
    return _entities;
}

Entity const &World::getEntityById(string const &id) const
{
    return _entities.at(id);
}

Camera const &World::getCameraById(string const &id) const
{
    return _cameras.at(id);
}

Camera const &World::getActiveCamera() const
{
    return getCameraById(_activeCameraId);
}

bool World::getRegisteredByRenderer() const
{
    return _registeredByRenderer;
}

void World::setActiveCameraById(string const &id)
{
    if (getCameraById(id).getId() != "None")
        _activeCameraId = id;
}

void World::setRegisteredByRenderer(bool registered)
{
    _registeredByRenderer = registered;
}

void World::setRendererId(string const &entityId, string const &entityPartId, string const &name, int value)
{
    auto &entity = _entities.at(entityId);
    entity.setRendererId(entityPartId, name, value);
}

void World::updatePlayer(Player const &newPlayer)
{
    _player = newPlayer;
}

void World::updateCameraById(string id, Camera const &newCamera)
{
    auto &camera = _cameras.at(id);
    camera = newCamera;
}

void World::updateEntityById(string id, Entity const &newEntity)
{
    auto &entity = _entities.at(id);
    entity = newEntity;
}

void World::clearEntities()
{
    _entities.clear();
}

void World::clearCameras()
{
    _cameras.clear();
    _activeCameraId = "None";
}

void World::clearWorld()
{
    clearEntities();
    clearCameras();
}

void World::resetWorld(string const &worldFileName)
{

    // read json file
    nlohmann::json jsonContents;
    ifstream file(worldFileName);
    file >> jsonContents;

    cout << "Read JSON file \"" << worldFileName << "\"." << endl;

    resetCameras(jsonContents);
    resetPlayer(jsonContents);
    resetCollectibleEntities(jsonContents);
}

void World::resetPlayer(nlohmann::json const &jsonContents)
{
    if (jsonContents.count("playerEntity") != 0)
    {
        cout << "Resetting player from world file" << endl;

        auto jsonPlayer = jsonContents.at("playerEntity");
        auto playerEntity = getEntityById("Player");

        if (jsonPlayer.count("absolutePosition") != 0)
        {
            float x = jsonPlayer.at("absolutePosition")[0];
            float y = jsonPlayer.at("absolutePosition")[1];
            float z = jsonPlayer.at("absolutePosition")[2];
            playerEntity.setAbsolutePosition(x, y, z); // TODO: should this be player.moveTo?
        }

        playerEntity.resetRotation();
        
        if (jsonPlayer.count("rotationDegreesZ") != 0)
            playerEntity.setRotationDegreesZ(jsonPlayer.at("rotationDegreesZ"));

        if (jsonPlayer.count("rotationDegreesY") != 0)
            playerEntity.setRotationDegreesY(jsonPlayer.at("rotationDegreesY"));

        if (jsonPlayer.count("rotationDegreesX") != 0)
            playerEntity.setRotationDegreesX(jsonPlayer.at("rotationDegreesX"));

        updateEntityById("Player", playerEntity);
    }
}

void World::resetCollectibleEntities(nlohmann::json const &jsonContents)
{
    if (jsonContents.count("entities") != 0)
    {
        // add each entity
        for (auto const &jsonEntity : jsonContents.at("entities"))
        {
            if (jsonEntity.count("id") != 0 && jsonEntity.count("isCollectible") != 0)
            {
                auto entity = getEntityById(jsonEntity.at("id"));

                if (jsonEntity.count("absolutePosition") != 0)
                {
                    float x = jsonEntity.at("absolutePosition")[0];
                    float y = jsonEntity.at("absolutePosition")[1];
                    float z = jsonEntity.at("absolutePosition")[2];
                    entity.setAbsolutePosition(x, y, z);
                }

                if (jsonEntity.count("rotationDegreesZ") != 0)
                    entity.setRotationDegreesZ(jsonEntity.at("rotationDegreesZ"));

                if (jsonEntity.count("rotationDegreesY") != 0)
                    entity.setRotationDegreesY(jsonEntity.at("rotationDegreesY"));

                if (jsonEntity.count("rotationDegreesX") != 0)
                    entity.setRotationDegreesX(jsonEntity.at("rotationDegreesX"));

                updateEntityById(jsonEntity.at("id"), entity);
            }
        }

        cout << "Added entities from world file" << endl;
    }
}

void World::resetCameras(nlohmann::json const &jsonContents)
{
    clearCameras();
    addCamerasFromWorldFile(jsonContents);
}