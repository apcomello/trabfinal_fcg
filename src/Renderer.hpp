#ifndef __RENDERER_HPP__
#define __RENDERER_HPP__

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "World.hpp"
#include "TextRenderer.hpp"

class Renderer
{
private:
  GLFWwindow *_window;
  TextRenderer _textRenderer;
  std::map<std::string, GLuint> _shaderPrograms;
  std::map<std::string, GLuint> _textureImages;
  GLuint _activeShaderProgram;
  int _screenWidth;
  int _screenHeight;
  bool _showObjectives;
  bool _showStartScreen;
  std::string _gameOverMessage;
  std::vector<std::string> _objectiveList;

public:
  Renderer(std::string gameName, int width, int height);
  ~Renderer();

  GLFWwindow &getWindow();

  void printGLInfo() const;
  void loadShaderProgramsFromShaderCfgFile(std::string fileName);
  void loadTexturesFromTextureAtlasFile(std::string fileName);
  void registerWorld(World &world);
  void render(World &world, int counter, std::vector<std::string> objectiveList, bool validGame, bool objectiveCompleted);
  void renderEntity(World const &world, Camera const &camera, Entity const &entity, glm::vec4 lightPosition);
  void renderEntityPart(Camera const &camera, Entity const &entity, Entity const *sourceEntity, EntityPart const &part, glm::vec4 lightPosition);
  void setActiveShaderProgram(std::string id);
  void toggleObjectiveList(bool showObjective);
  void hideStartScreen();
  void showStartScreen();
  void setGameOverMessage(std::string);

private:
  static void errorCallback(int error, const char *description);
};

#endif // __RENDERER_HPP__
