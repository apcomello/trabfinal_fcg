#ifndef __GAME_HPP__
#define __GAME_HPP__

#include <string>
#include <vector>
#include "Renderer.hpp"

class Game
{
private:
  Renderer _renderer;
  std::vector<std::string> _objectiveList;
  bool _validGame;
  bool _objectiveCompleted;
  bool _gameStarted;
  int _objectsCollected;

public:
  Game(std::string gameName);
  bool isInObjectiveList(std::string objectId);
  void createObjectiveList(World world);
  void restartGame();
  void run();
};

#endif // __GAME_HPP__
