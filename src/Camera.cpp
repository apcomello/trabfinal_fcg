#include <iostream>
#include "Camera.hpp"
#include "Matrix.hpp"

using namespace std;

Camera::Camera(string id)
{
    _id = id;
    _cameraType = CameraType::LOOK_AT;
    _projectionType = ProjectionType::PERSPECTIVE;
    _fieldOfView = 3.14159265 / 3;
    _nearPlane = -0.1;
    _farPlane = -10;
    _aspectRatio = 1;
    _lookAtPosition = glm::vec4(0.0, 0.0, 0.0, 1.0);
    _position = glm::vec4(0.0, 0.0, 2.5, 1.0);
    _cumulativeDisplacement = glm::vec4(0.0, 0.0, 0.0, 0.0);

    if (_cameraType == CameraType::FREE_FIXED || _cameraType == CameraType::FREE_DEBUG)
    {
        _viewVector = glm::vec4(1.0, 0.0, 0.0, 0.0);
        getSphericalCoordinates(1.0, 0.0, 0.0);
    }
    else if (_cameraType == CameraType::LOOK_AT)
    {
        _viewVector = Matrix::normalize(_lookAtPosition - _position);
        getSphericalCoordinates(_position.x, _position.y, _position.z);
    }

    _upVector = glm::vec4(0.0, 1.0, 0.0, 0.0);
    _cameraDistance = 2.0;
    _rightVector = Matrix::crossProduct(_viewVector, _upVector);
}

void Camera::displace(float x, float y, float z, float deltaMovement, float deltaTime)
{
    // Movement in the xz plane only
    auto projectedDisplacement = glm::vec4(x, 0.0, z, 0.0);
    _position.x += projectedDisplacement.x * deltaMovement * deltaTime;
    _position.z += projectedDisplacement.z * deltaMovement * deltaTime;

    _cumulativeDisplacement.x += projectedDisplacement.x * deltaMovement * deltaTime;
    _cumulativeDisplacement.z += projectedDisplacement.z * deltaMovement * deltaTime;

    if (_cameraType == CameraType::LOOK_AT)
    {
        _lookAtPosition.x += projectedDisplacement.x * deltaMovement * deltaTime;
        _lookAtPosition.z += projectedDisplacement.z * deltaMovement * deltaTime;

        _viewVector = Matrix::normalize(_lookAtPosition - _position);
    }

    _rightVector = Matrix::crossProduct(_viewVector, _upVector);
}

void Camera::turn(float dx, float dy, float deltaTime)
{
    // Limits how much the camera can turn in the y axis
    float phimax = 1;
    float phimin = 0.05;

    _theta -= 0.01f * dx;
    _phi += 0.01f * dy;

    if (_phi > phimax)
        _phi = phimax;

    if (_phi < phimin)
        _phi = phimin;

    // Spherical to cartesian coordinates
    float x = _cameraDistance * cos(_phi) * sin(_theta);
    float y = _cameraDistance * sin(_phi);
    float z = _cameraDistance * cos(_phi) * cos(_theta);

    if (_cameraType == CameraType::FREE_FIXED || _cameraType == CameraType::FREE_DEBUG)
    {
        _viewVector = glm::vec4(x, -y, z, 0.0);
    }
    else if (_cameraType == CameraType::LOOK_AT)
    {
        _position = Matrix::matrixTranslate(_cumulativeDisplacement.x, _cumulativeDisplacement.y, _cumulativeDisplacement.z) *
                    glm::vec4(x, y, z, 1.0);
        _viewVector = Matrix::normalize(_lookAtPosition - _position);
    }

    _rightVector = Matrix::crossProduct(_viewVector, _upVector);
}

string const &Camera::getId() const
{
    return _id;
}

float Camera::getFieldOfView() const
{
    return _fieldOfView;
}

float Camera::getNearPlane() const
{
    return _nearPlane;
}

float Camera::getFarPlane() const
{
    return _farPlane;
}

float Camera::getAspectRatio() const
{
    return _aspectRatio;
}

glm::vec4 Camera::getPosition() const
{
    return _position;
}

glm::vec4 Camera::getViewVector() const
{
    return _viewVector;
}

glm::vec4 Camera::getRightVector() const
{
    return _rightVector;
}

glm::vec4 Camera::getUpVector() const
{
    return _upVector;
}

glm::vec4 Camera::getLookAtPosition() const
{
    return _lookAtPosition;
}

CameraType Camera::getCameraType() const
{
    return _cameraType;
}

ProjectionType Camera::getProjectionType() const
{
    return _projectionType;
}

glm::mat4 Camera::getViewMatrix() const
{
    return Matrix::matrixCameraView(_position, _viewVector, _upVector);
}

glm::mat4 Camera::getProjectionMatrix() const
{
    if (_projectionType == ProjectionType::PERSPECTIVE)
        return Matrix::matrixPerspective(_fieldOfView, _aspectRatio, _nearPlane, _farPlane);
    else
        return Matrix::matrixOrthographic(_cameraDistance, _aspectRatio, _nearPlane, _farPlane);
}

float Camera::getTheta()
{
    return _theta;
}

float Camera::getPhi()
{
    return _phi;
}

void Camera::setId(string const &id)
{
    _id = id;
}

void Camera::setFieldOfView(float fieldOfView)
{
    _fieldOfView = fieldOfView;
}

void Camera::setNearPlane(float nearPlane)
{
    _nearPlane = nearPlane;
}

void Camera::setFarPlane(float farPlane)
{
    _farPlane = farPlane;
}

void Camera::setAspectRatio(float aspectRatio)
{
    _aspectRatio = aspectRatio;
}

void Camera::setPosition(float x, float y, float z)
{
    _position = glm::vec4(x, y, z, 1.0f);

    if (_cameraType == CameraType::LOOK_AT)
    {
        _viewVector = Matrix::normalize(_lookAtPosition - _position);
        getSphericalCoordinates(x, y, z);
    }
}

void Camera::setViewVector(float x, float y, float z)
{
    if (_cameraType == CameraType::FREE_FIXED || _cameraType == CameraType::FREE_DEBUG)
    {
        if (x == 0 && y == 0 && z == 0)
        {
            cerr << "WARNING: View vector cannot have zero length." << endl;
        }
        else
        {
            _viewVector = Matrix::normalize(glm::vec4(x, y, z, 0.0f));
            getSphericalCoordinates(_viewVector.x, _viewVector.y, _viewVector.z);
        }
    }
}

void Camera::setUpVector(float x, float y, float z)
{
    if (x == 0 && y == 0 && z == 0)
        cerr << "WARNING: Up vector cannot have zero length." << endl;
    else
        _upVector = glm::vec4(x, y, z, 0.0f);
}

void Camera::setLookAtPosition(float x, float y, float z)
{
    _lookAtPosition = glm::vec4(x, y, z, 1.0f);

    if (_cameraType == CameraType::LOOK_AT)
        _viewVector = Matrix::normalize(_lookAtPosition - _position);
}

void Camera::setOriginalLookAtPosition(float x, float y, float z)
{
    _originalLookAtPosition = glm::vec4(x, y, z, 1.0f);
}

void Camera::setCameraType(CameraType cameraType)
{
    _cameraType = cameraType;

    if (_cameraType == CameraType::LOOK_AT) {
        getSphericalCoordinates(_position.x, _position.y, _position.z);
    }
    else if (_cameraType == CameraType::FREE_FIXED || _cameraType == CameraType::FREE_DEBUG)
    {
        getSphericalCoordinates(_viewVector.x, _viewVector.y, _viewVector.z);
    }
}

void Camera::setProjectionType(ProjectionType projectionType)
{
    _projectionType = projectionType;
}

void Camera::setTheta(float theta)
{
    _theta = theta;
}

void Camera::setPhi(float phi)
{
    _phi = phi;
}

void Camera::getSphericalCoordinates(float x, float y, float z)
{
    _cameraDistance = sqrt(((x*x) + (y*y) + (z*z)));
    _phi = asin(y/_cameraDistance);
    _theta = atan2(x, z);
}
