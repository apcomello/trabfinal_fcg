#ifndef __MATRIX_HPP__
#define __MATRIX_HPP__

#include "glm/gtc/type_ptr.hpp"

class Matrix
{
public:
  static glm::mat4 matrix(
      float m00, float m01, float m02, float m03,
      float m10, float m11, float m12, float m13,
      float m20, float m21, float m22, float m23,
      float m30, float m31, float m32, float m33);
  static glm::mat4 matrixIdentity();
  static glm::mat4 matrixTranslate(float tx, float ty, float tz);
  static glm::mat4 matrixTranslate(glm::vec3 t);
  static glm::mat4 matrixScale(float sx, float sy, float sz);
  static glm::mat4 matrixScale(glm::vec3 s);
  static glm::mat4 matrixRotateDegreesX(float degrees);
  static glm::mat4 matrixRotateDegreesY(float degrees);
  static glm::mat4 matrixRotateDegreesZ(float degrees);
  static glm::mat4 matrixRotateDegrees(float degrees, glm::vec4 axis);
  static float norm(glm::vec3 v);
  static float norm(glm::vec4 v);
  static glm::vec3 normalize(glm::vec3 v);
  static glm::vec4 normalize(glm::vec4 v);
  static glm::vec3 crossProduct(glm::vec3 u, glm::vec3 v);
  static glm::vec4 crossProduct(glm::vec4 u, glm::vec4 v);
  static float dotProduct(glm::vec4 u, glm::vec4 v);
  static glm::vec3 elementwiseMin(glm::vec3 u, glm::vec3 v);
  static glm::vec4 elementwiseMin(glm::vec4 u, glm::vec4 v);
  static glm::vec3 elementwiseMax(glm::vec3 u, glm::vec3 v);
  static glm::vec4 elementwiseMax(glm::vec4 u, glm::vec4 v);
  static glm::vec3 elementwiseAbs(glm::vec3 v);
  static glm::vec4 elementwiseAbs(glm::vec4 v);
  static glm::vec3 elementwiseClamp(glm::vec3 v, glm::vec3 low, glm::vec3 high);
  static glm::vec4 elementwiseClamp(glm::vec4 v, glm::vec4 low, glm::vec4 high);
  static glm::mat4 matrixCameraView(glm::vec4 positionC, glm::vec4 viewVector, glm::vec4 upVector);
  static glm::mat4 matrixOrthographic(float top, float aspectRatio, float nearPlane, float farPlane);
  static glm::mat4 matrixPerspective(float fieldOfView, float aspectRatio, float nearPlane, float farPlane);
  static void printMatrix(glm::mat4 mat);
  static void printVector(glm::vec3 vec);
  static void printVector(glm::vec4 vec);
};

#endif // __MATRIX_HPP__
