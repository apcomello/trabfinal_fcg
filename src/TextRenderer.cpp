#include "dejavufont.h"
#include "TextRenderer.hpp"
#include "ShaderHelper.hpp"
#include "TextureHelper.hpp"
#include <iostream>

using namespace std;

TextRenderer::TextRenderer()
{
    _textScale = 1.5f;
}

void TextRenderer::textRendererInit(int shaderProgramId)
{
    _shaderProgramId = shaderProgramId;

    glGenVertexArrays(1, &_textVAO);

    glBindVertexArray(_textVAO);

    // allocate openGL sampler, texture and vertex array
    glGenSamplers(1, &_samplerId);
    glGenTextures(1, &_textureId);
    glGenBuffers(1, &_textVBO);

    // set wrapping and mipmapping
    glSamplerParameteri(_samplerId, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(_samplerId, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(_samplerId, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(_samplerId, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // reserve a texture for the font
    _textureHelperId = TextureHelper::allocateTextureHelperId();
    glActiveTexture(GL_TEXTURE0 + _textureHelperId);
    glBindTexture(GL_TEXTURE_2D, _textureId);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_R8, dejavufont.tex_width, dejavufont.tex_height, 0, GL_RED, GL_UNSIGNED_BYTE, dejavufont.tex_data);
    glBindSampler(_textureHelperId, _samplerId);

    // reserve a VBO for the font
    glBindBuffer(GL_ARRAY_BUFFER, _textVBO);
    glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void TextRenderer::printString(const string &str, float x, float y, int screenWidth, int screenHeight, float scale)
{
    float sx = scale * _textScale / screenWidth;
    float sy = scale * _textScale / screenHeight;

    for (size_t i = 0; i < str.size(); i++)
    {
        // find the glyph for the character we are looking for
        texture_glyph_t *glyph = 0;
        for (size_t j = 0; j < dejavufont.glyphs_count; ++j)
        {
            if (dejavufont.glyphs[j].codepoint == (unsigned)str[i])
            {
                glyph = &dejavufont.glyphs[j];
                break;
            }
        }

        if (!glyph)
            continue;

        x += glyph->kerning[0].kerning;
        float x0 = (float) (x + glyph->offset_x * sx);
        float y0 = (float) (y + glyph->offset_y * sy);
        float x1 = (float) (x0 + glyph->width * sx);
        float y1 = (float) (y0 - glyph->height * sy);

        float s0 = glyph->s0 - 0.5f/dejavufont.tex_width;
        float t0 = glyph->t0 - 0.5f/dejavufont.tex_height;
        float s1 = glyph->s1 - 0.5f/dejavufont.tex_width;
        float t1 = glyph->t1 - 0.5f/dejavufont.tex_height;

        struct {float x, y, s, t;} data[6] = {
            { x0, y0, s0, t0 },
            { x0, y1, s0, t1 },
            { x1, y1, s1, t1 },
            { x0, y0, s0, t0 },
            { x1, y1, s1, t1 },
            { x1, y0, s1, t0 }
        };

        // setup GL
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDepthFunc(GL_ALWAYS);

        // feed the data
        glBindBuffer(GL_ARRAY_BUFFER, _textVBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, 24 * sizeof(float), data);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // switch to our shader program
        GLint currentProgramId;
        glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgramId);
        glUseProgram(_shaderProgramId);

        // draw the character
        glBindVertexArray(_textVAO);
        glBindTexture(GL_TEXTURE_2D, _textureId);
        glUniform1i(glGetUniformLocation(_shaderProgramId, "u_font_texture"), _textureHelperId);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // restore the program
        glUseProgram(currentProgramId);
        // restore GL
        glDepthFunc(GL_LESS);

        x += (glyph->advance_x * sx);
    }
}

void TextRenderer::printTimer(int timer, float x, float y, int screenWidth, int screenHeight, float scale)
{
    int minutes = timer / 60;
    int seconds = timer % 60;
    char buffer[20] = "--:--";
    snprintf(buffer, 20, "%02d:%02d", minutes, seconds);
    printString(buffer, x, y, screenWidth, screenHeight, scale);
}

void TextRenderer::printStartScreen(int screenWidth, int screenHeight)
{
    printString("Grand Theft Artsy", -0.5, 0.2, screenWidth, screenHeight, 3);
    printString("Press ENTER to start the game", -0.3, 0.1, screenWidth, screenHeight);
    printString("Press TAB to see your objective", -0.3, 0, screenWidth, screenHeight);
}

float TextRenderer::getLineHeight(int screenHeight)
{
    return dejavufont.height / screenHeight * _textScale;
}

float TextRenderer::getCharWidth(int screenWidth)
{
    return dejavufont.glyphs[32].advance_x / screenWidth * _textScale;
}
