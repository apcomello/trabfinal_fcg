#include <iostream>
#include <fstream>
#include <sstream>
#include "ShaderHelper.hpp"

using namespace std;

GLuint ShaderHelper::createGPUProgram(string const &vertexShaderFileName,
                                      string const &fragmentShaderFileName)
{
    GLuint vertexShaderId = loadVertexShader(vertexShaderFileName);
    GLuint fragmentShaderId = loadFragmentShader(fragmentShaderFileName);

    // link both shaders into the program
    GLuint programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);

    // check linking
    GLint linked_ok;
    glGetProgramiv(programId, GL_LINK_STATUS, &linked_ok);
    if (!linked_ok)
    {
        // print linking log
        GLint log_length;
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &log_length);
        GLchar *log = new GLchar[log_length];
        glGetProgramInfoLog(programId, log_length, &log_length, log);

        cerr << "ERROR: OpenGL program linking failed." << endl;
        cerr << "== Start of linking log" << endl;
        cerr << log << endl;
        cerr << "== End of link log" << endl;

        delete[] log;
    }

    // delete shader objects
    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);

    return programId;
}

GLuint ShaderHelper::loadVertexShader(string const &vertexShaderFileName)
{
    GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);

    loadShader(vertexShaderFileName, vertexShaderId);

    return vertexShaderId;
}

GLuint ShaderHelper::loadFragmentShader(string const &fragmentShaderFileName)
{
    GLuint fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

    loadShader(fragmentShaderFileName, fragmentShaderId);

    return fragmentShaderId;
}

void ShaderHelper::loadShader(string const &shaderFileName, GLuint shaderId)
{
    cout << "Loading shader file \"" << shaderFileName << "\"..." << endl;

    ifstream file;

    try
    {
        file.exceptions(ifstream::failbit);
        file.open(shaderFileName);
    }
    catch (exception &e)
    {
        cerr << "ERROR: Cannot open file \"" << shaderFileName << "\"." << endl;
        exit(EXIT_FAILURE);
    }

    stringstream shader;
    shader << file.rdbuf();
    string str = shader.str();

    // send openGL the shader's source code
    const GLchar *shader_string = str.c_str();
    const GLint shader_string_length = str.length();
    glShaderSource(shaderId, 1, &shader_string, &shader_string_length);
    glCompileShader(shaderId);

    // check compilation
    GLint compiled_ok;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compiled_ok);
    if (!compiled_ok)
    {
        // print compilation log
        GLint log_length;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &log_length);
        GLchar *log = new GLchar[log_length];
        glGetShaderInfoLog(shaderId, log_length, &log_length, log);

        cerr << (compiled_ok ? "WARNING" : "ERROR") << ": ";
        cerr << "OpenGL compilation of \"" << shaderFileName << "\"." << endl;
        cerr << "== Start of compilation log" << endl;
        cerr << log << endl;
        cerr << "== End of compilation log" << endl;

        delete[] log;
    }

    cout << "Loaded shader file \"" << shaderFileName << "\"." << endl;
}
