#ifndef __CAMERA_TYPE_HPP__
#define __CAMERA_TYPE_HPP__

enum class CameraType
{
    FREE_DEBUG,
    FREE_FIXED,
    LOOK_AT,
};

#endif // __CAMERA_TYPE_HPP__