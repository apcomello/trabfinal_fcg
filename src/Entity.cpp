#include <algorithm>
#include <iostream>
#include "tiny_obj_loader.h"
#include "Entity.hpp"
#include "Matrix.hpp"

using namespace std;

Entity::Entity(string const &id, string const &cloneFromId)
{
    _id = id;
    _cloneFromId = cloneFromId;
    _bboxShape = BBoxShape::CUBE;
    _absolutePosition = glm::vec4(0.0, 0.0, 0.0, 1.0);
    _scaleFactorX = 1.0;
    _scaleFactorY = 1.0;
    _scaleFactorZ = 1.0;
    _rotationDegreesX = 0.0;
    _rotationDegreesY = 0.0;
    _rotationDegreesZ = 0.0;
    _isCollectible = false;
}

string const &Entity::getId() const
{
    return _id;
}

string const &Entity::getCloneFromId() const
{
    return _cloneFromId;
}

map<string, EntityPart> const &Entity::getEntityParts() const
{
    return _parts;
}

EntityPart const &Entity::getEntityPartById(string const &id) const
{
    return _parts.at(id);
}

glm::vec4 Entity::getBBoxMin() const
{
    auto transformMatrix = getAbsoluteTransform();
    auto transformedMin = transformMatrix * _modelTransformedBBoxMin;
    auto transformedMax = transformMatrix * _modelTransformedBBoxMax;

    return Matrix::elementwiseMin(transformedMin, transformedMax);
}

glm::vec4 Entity::getBBoxMinNoTranform() const
{
    return _modelTransformedBBoxMin;
}

glm::vec4 Entity::getBBoxMax() const
{
    auto transformMatrix = getAbsoluteTransform();
    auto transformedMin = transformMatrix * _modelTransformedBBoxMin;
    auto transformedMax = transformMatrix * _modelTransformedBBoxMax;

    return Matrix::elementwiseMax(transformedMin, transformedMax);
}

glm::vec4 Entity::getBBoxMaxNoTranform() const
{
    return _modelTransformedBBoxMax;
}

BBoxShape Entity::getBBoxShape() const
{
    return _bboxShape;
}

glm::vec4 Entity::getAbsolutePosition() const
{
    return _absolutePosition;
}

float Entity::getScaleFactorX() const
{
    return _scaleFactorX;
}

float Entity::getScaleFactorY() const
{
    return _scaleFactorY;
}

float Entity::getScaleFactorZ() const
{
    return _scaleFactorZ;
}

float Entity::getRotationDegreesX() const
{
    return _rotationDegreesX;
}

float Entity::getRotationDegreesY() const
{
    return _rotationDegreesY;
}

float Entity::getRotationDegreesZ() const
{
    return _rotationDegreesZ;
}

glm::mat4 Entity::getAbsoluteTransform() const
{
    // same order as in Lab3 to retain compatibility
    return Matrix::matrixTranslate(_absolutePosition)
         * Matrix::matrixRotateDegreesZ(_rotationDegreesZ)
         * Matrix::matrixRotateDegreesY(_rotationDegreesY)
         * Matrix::matrixRotateDegreesX(_rotationDegreesX)
         * Matrix::matrixScale(_scaleFactorX, _scaleFactorY, _scaleFactorZ);
}

int Entity::getRendererId(string const &name) const
{
    if (_rendererIds.count(name) == 0)
        return -1;

    return _rendererIds.at(name);
}

bool Entity::isCollectible() const
{
    return _isCollectible;
}

void Entity::setCollectibleProperty(bool isCollectible)
{
    _isCollectible = isCollectible;
}

void Entity::setId(string const &id)
{
    _id = id;
}

void Entity::setCloneFromId(string const &cloneFromId)
{
    _cloneFromId = cloneFromId;
}

void Entity::addEntityPart(EntityPart part)
{
    pair<string, EntityPart> mappedPart(part.getId(), part);
    _parts.insert(mappedPart);

    // below we update the entity's bbox
    // start by locating the source for the geometry
    auto *sourcePart = &part;
    while (sourcePart->getCloneFromId() != "None")
        sourcePart = &_parts.at(sourcePart->getCloneFromId());

    // transform this geometry's bbox using our relative transformation
    auto modelBBoxMin = part.getRelativeTransform() * sourcePart->getBBoxMinNoTranform();
    auto modelBBoxMax = part.getRelativeTransform() * sourcePart->getBBoxMaxNoTranform();

    // update bbox min and max
    if (_parts.size() == 1)
    {
        _modelTransformedBBoxMin = modelBBoxMin;
        _modelTransformedBBoxMax = modelBBoxMax;
    }
    else
    {
        _modelTransformedBBoxMin = Matrix::elementwiseMin(_modelTransformedBBoxMin, modelBBoxMin);
        _modelTransformedBBoxMax = Matrix::elementwiseMax(_modelTransformedBBoxMax, modelBBoxMax);
    }
}

void Entity::clearParts()
{
    _parts.clear();
}

void Entity::setBBoxShape(BBoxShape bboxShape)
{
    _bboxShape = bboxShape;
}

void Entity::setAbsolutePosition(float x, float y, float z)
{
    _absolutePosition = glm::vec4(x, y, z, 1.0f);
}

void Entity::setScaleFactorX(float scaleFactorX)
{
    _scaleFactorX = scaleFactorX;
}

void Entity::setScaleFactorY(float scaleFactorY)
{
    _scaleFactorY = scaleFactorY;
}

void Entity::setScaleFactorZ(float scaleFactorZ)
{
    _scaleFactorZ = scaleFactorZ;
}

void Entity::setRotationDegreesX(float rotationDegreesX)
{
    _rotationDegreesX = rotationDegreesX;
}

void Entity::setRotationDegreesY(float rotationDegreesY)
{
    _rotationDegreesY = rotationDegreesY;
}

void Entity::setRotationDegreesZ(float rotationDegreesZ)
{
    _rotationDegreesZ = rotationDegreesZ;
}

void Entity::resetRotation()
{
    setRotationDegreesX(0.0);
    setRotationDegreesY(0.0);
    setRotationDegreesZ(0.0);
}

void Entity::setRendererId(string const &name, int value)
{
    _rendererIds[name] = value;
}

void Entity::setRendererId(string const &entityPartId, string const &name, int value)
{
    auto &part = _parts.at(entityPartId);
    part.setRendererId(name, value);
}

void Entity::updateEntityPartById(string const &id, EntityPart const &newEntityPart)
{
    auto &entityPart = _parts.at(id);
    entityPart = newEntityPart;
}

Entity Entity::fromObjFile(string const &objFileName)
{
    // load the model file into tinyobj
    tinyobj::attrib_t attrib;
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;
    string err;

    cout << "Loading model file \"" << objFileName << "\"..." << endl;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, objFileName.c_str(), "../../data/obj/", true);

    if (!ret)
    {
        cerr << "ERROR: Error loading model." << endl;

        if (!err.empty())
            cerr << err << endl;

        return Entity("None");
    }

    cout << "Loaded model file \"" << objFileName << "\"." << endl;

    cout << "Model has " << shapes.size() << " parts" << endl;

    // create our new entity
    Entity entity("From file " + objFileName);

    // we'll run the shape-face loop twice:
    // once we validate the object and generate all the vertex normals
    // if needed; second time we add everything to the entity;
    // this is so ugly and hacky... gosh, this file format sucks

    // temporary vertex normals, all initially set to zero
    int numVertices = attrib.vertices.size();
    vector<glm::vec3> vertexNormals(numVertices, glm::vec3(0.0, 0.0, 0.0));

    // for each shape
    for (auto const &shape : shapes)
    {
        // for each face in the shape
        int numFaces = shape.mesh.num_face_vertices.size();
        for (int f = 0; f < numFaces; ++f)
        {
            // validate that the face is a triangle
            int faceNumVertices = shape.mesh.num_face_vertices[f];
            if (faceNumVertices != 3)
            {
                cerr << "ERROR: Model's faces are not only triangles!" << endl <<
                        "Found a face with " << faceNumVertices << " vertices!" << endl;

                return Entity("None");
            }

            // if we already have normals, skip the next section
            // what it does is generate vertex normals by averaging face normals
            // (Gouraud shading) in case we don't have them in the obj file
            if (attrib.normals.size() > 0)
                continue;

            // temporary triangle vertices and indices
            vector<glm::vec3> triangleVertices;
            vector<int> triangleIndices;

            // for each vertex in the face
            for (int v = 0; v < 3; ++v)
            {
                tinyobj::index_t idx = shape.mesh.indices[3*f + v];

                // add the vertex to the temporary triangle
                float vx = attrib.vertices[3*idx.vertex_index + 0];
                float vy = attrib.vertices[3*idx.vertex_index + 1];
                float vz = attrib.vertices[3*idx.vertex_index + 2];
                triangleVertices.push_back(glm::vec3(vx, vy, vz));

                // add the index to the temporary triangle
                triangleIndices.push_back(idx.vertex_index);
            }

            // calculate the triangle's normal
            glm::vec3 a = triangleVertices[0];
            glm::vec3 b = triangleVertices[1];
            glm::vec3 c = triangleVertices[2];
            glm::vec3 n = Matrix::crossProduct(b - a, c - a);

            // add the triangle's normal to each vertex normal
            vertexNormals.at(triangleIndices[0]) += n;
            vertexNormals.at(triangleIndices[1]) += n;
            vertexNormals.at(triangleIndices[2]) += n;
        }
    }

    // normalize vertex normals to average out the accumulated triangle normals
    for (auto &normal : vertexNormals)
        normal = Matrix::normalize(normal);

    // run the loop once again, this time adding everything

    // for each shape
    int numShapes = shapes.size();
    for (int s = 0; s < numShapes; ++s)
    {
        // create a new entity part using the object's name as its ID
        EntityPart part(to_string(s));

        // convert from object vertex indices to entity part vertex indices
        // we need this because we only take the vertices we need for the part
        // and this forces us to re-index them differently
        vector<int> vertexReindex(numVertices, -1);
        int reindexedVertices = 0;

        // for each face in the shape
        int numFaces = shapes[s].mesh.num_face_vertices.size();
        for (int f = 0; f < numFaces; ++f)
        {
            // temporary triangle indices
            vector<int> triangleIndices;

            // for each vertex in the face
            for (int v = 0; v < 3; ++v)
            {
                tinyobj::index_t idx = shapes[s].mesh.indices[3*f + v];
                int vertexIndex = idx.vertex_index;

                // check if this is a known vertex for the entity part
                // if so, reuse a previous index; if not, allocate a new one
                if (vertexReindex.at(vertexIndex) != -1)
                {
                    // add the previously created index to the triangle
                    int triangleIndex = vertexReindex.at(vertexIndex);
                    triangleIndices.push_back(triangleIndex);
                }
                else
                {
                    // add the new vertex
                    float vx = attrib.vertices[3*vertexIndex + 0];
                    float vy = attrib.vertices[3*vertexIndex + 1];
                    float vz = attrib.vertices[3*vertexIndex + 2];
                    part.addVertex(vx, vy, vz);

                    // add either an existing normal or the calculated one
                    if (attrib.normals.size() > 0 && idx.normal_index != -1)
                    {
                        float nx = attrib.normals[3*idx.normal_index + 0];
                        float ny = attrib.normals[3*idx.normal_index + 1];
                        float nz = attrib.normals[3*idx.normal_index + 2];
                        part.addVertexNormal(nx, ny, nz);
                    }
                    else
                    {
                        auto &normal = vertexNormals[vertexIndex];
                        part.addVertexNormal(normal.x, normal.y, normal.z);
                    }

                    // add the texture UV coordinates
                    if (idx.texcoord_index != -1)
                    {
                        float U = attrib.texcoords[2*idx.texcoord_index + 0];
                        float V = attrib.texcoords[2*idx.texcoord_index + 1];
                        part.addVertexUV(U, V);
                    }

                    // add the new index to the triangle
                    int newIndex = reindexedVertices;
                    triangleIndices.push_back(newIndex);

                    // mark this vertex as a known vertex
                    vertexReindex[vertexIndex] = newIndex;
                    reindexedVertices += 1;
                }
            }

            // add the triangle's indices
            part.addTriangle(triangleIndices[0], triangleIndices[1], triangleIndices[2]);

            // if it has a material, set it, even if it's already been set before
            // we do this because obj files have per-face materials, which GL doesn't support
            if (materials.size() > 0)
            {
                int materialId = shapes[s].mesh.material_ids[f];
                if (materialId != -1)
                {
                    auto const &material = materials.at(materialId);
                    part.setAmbientColor(glm::make_vec3(material.ambient));
                    part.setDiffuseColor(glm::make_vec3(material.diffuse));
                    part.setSpecularColor(glm::make_vec3(material.specular));
                    part.setShininess(material.shininess);
                    part.setOpacity(material.dissolve);
                }
            }
        }

        // add the new part
        entity.addEntityPart(part);
    }

    return entity;
}
