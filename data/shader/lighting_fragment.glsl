#version 330 core

in vec4 position_model;
in vec4 position_world;
in vec4 normal;
in vec2 uv;
in vec3 v_color;

uniform mat4 u_view;
uniform vec3 u_ambient_color;
uniform vec3 u_diffuse_color;
uniform vec3 u_specular_color;
uniform float u_shininess;
uniform float u_opacity;
uniform bool u_has_texture;
uniform bool u_is_texture_normal_map;
uniform bool u_has_uv;
uniform sampler2D u_texture_id;
uniform vec4 u_bbox_min;
uniform vec4 u_bbox_max;
uniform vec4 u_light_position;
uniform bool u_use_gouraud;

out vec4 color;

void main()
{
    if (u_use_gouraud)
    {
        color.rgb = v_color;
    }
    else
    {
        // object params
        vec3 Kd = u_diffuse_color;
        vec3 Ks = u_specular_color;
        vec3 Ka = u_ambient_color;
        float q = u_shininess;
        vec4 p = position_world;
        vec4 n = normalize(normal);

        // alter object params from images
        if (u_has_texture)
        {
            // we have a normal map and UVs
            if (u_is_texture_normal_map && u_has_uv)
            {
                // read normal using UVs
                n.rgb = texture(u_texture_id, uv).rgb;
                n = normalize(n * 2.0 - 1.0);
            }

            // we have a regular texture
            else if (!u_is_texture_normal_map)
            {
                if (u_has_uv)
                {
                    // read color using UVs
                    Kd = texture(u_texture_id, uv).rgb;
                }
                else
                {
                    // read color using calculated bbox coords
                    float u = (position_model.x - u_bbox_min.x) / (u_bbox_max.x - u_bbox_min.x);
                    float v = (position_model.y - u_bbox_min.y) / (u_bbox_max.y - u_bbox_min.y);
                    Kd = texture(u_texture_id, vec2(u, v)).rgb;
                }

                Ka = Kd / 2.0f;
            }
        }

        // camera
        vec4 origin = vec4(0.0, 0.0, 0.0, 1.0);
        vec4 camera_position = inverse(u_view) * origin;
        vec4 v = normalize(camera_position - p);

        // lighting
        vec4 l = u_light_position - p; // don't normalize yet
        float d = length(l); // first take the dist
        l = l / d;
        vec4 h = normalize(l + v);
        float atten = 1.0 / (1.0 + 10 * d * d);
        vec3 I = vec3(1.0, 1.0, 0.0) * atten;
        vec3 Ia = vec3(0.01, 0.01, 0.01);

        // resulting color
        // set specular color to (0, 0, 0) to disable glare
        vec3 lambert_diffuse_term = Kd * I * max(0.0, dot(n, l));
        vec3 ambient_term = Ka * Ia;
        vec3 blinn_phong_specular_term = Ks * I * pow(max(0.0, dot(n, h)), q);
        color.rgb = lambert_diffuse_term + ambient_term + blinn_phong_specular_term;
    }

    // gamma correction and alpha
    color.rgb = pow(color.rgb, vec3(1.0 / 2.2));
    color.a = u_opacity;
}
