#version 330 core

out vec4 color;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform vec4 u_color;

void main()
{
    color = vec4(0.0, 0.0, 0.0, 1.0);
}
