#version 330 core

uniform sampler2D u_font_texture;

in vec2 texCoords;

out vec4 fragColor;

void main()
{
    fragColor = vec4(1.0, 1.0, 1.0, texture(u_font_texture, texCoords).r);
}
