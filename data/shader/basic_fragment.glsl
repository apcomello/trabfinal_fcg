#version 330 core

in vec4 position_model;
in vec4 position_world;
in vec4 normal;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform vec4 u_color;
uniform bool u_has_texture;
uniform sampler2D u_texture_id;
uniform vec4 u_bbox_min;
uniform vec4 u_bbox_max;

out vec4 color;

void main()
{
    if (u_has_texture)
    {
        float u = (position_model.x - u_bbox_min.x) / (u_bbox_max.x - u_bbox_min.x);
        float v = (position_model.y - u_bbox_min.y) / (u_bbox_max.y - u_bbox_min.y);
        color = texture(u_texture_id, vec2(u, v));
    }
    else
    {
        color = u_color;
    }

    color.rgb = pow(color.rgb, vec3(1.0 / 2.2));
}
