#version 330 core

layout (location = 0) in vec4 model_coefficients;
layout (location = 1) in vec4 normal_coefficients;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

out vec4 position_model;
out vec4 position_world;
out vec4 normal;

void main()
{
    gl_Position = u_projection * u_view * u_model * model_coefficients;
    position_model = model_coefficients;
    position_world = u_model * model_coefficients;
    normal = inverse(transpose(u_model)) * normal_coefficients;
    normal.w = 0.0;
}
